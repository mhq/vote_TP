package com.elione.tools;


import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;


/**
 * 
 * 
 * @author mhq
 * @version 1.0 (26.06.2015)
 *
 */
public class ApplicationContextProvider implements ApplicationContextAware {
	
	
	private static ApplicationContext ctx = null;
	
	public static ApplicationContext getApplicationContext() {
		return ctx ;
	}
	
	@Override
	public void setApplicationContext(ApplicationContext context) throws BeansException {
		System.out.println( " Init context !" );
		ctx = context ;
	}
	
}
