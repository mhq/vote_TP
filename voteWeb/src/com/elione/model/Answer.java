package com.elione.model;

import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.elione.model.base.BaseModel;


/**
 * 
 * 投票的问题
 * 
 * @author 
 *
 */
@Entity
@Table(name="yh_answer")
@SequenceGenerator(name="sequenceGenerator", sequenceName="yh_answer_sequence")
public class Answer extends BaseModel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6288370599987702813L;
	
	private String answerValue ;	//9朝古都
	private Long questionID ;		//问题的编号
	
	public String getAnswerValue() {
		return answerValue;
	}
	public void setAnswerValue(String answerValue) {
		this.answerValue = answerValue;
	}
	public Long getQuestionID() {
		return questionID;
	}
	public void setQuestionID(Long questionID) {
		this.questionID = questionID;
	}
	
}
