package com.elione.model.base;

import java.io.Serializable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import org.hibernate.search.annotations.DocumentId;

/**
 *
 *
 * @author mhq
 *
 */
@MappedSuperclass
public class BaseModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8093848034143088820L;
 
	
	/** ID */
	private Long id;
	
	/**
	 * 获取ID
	 * 
	 * @return ID
	 */
	@DocumentId
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "sequenceGenerator")
	public Long getId() {
		return id;
	}
	
	/**
	 * 设置ID
	 * 
	 * @param id
	 *        ID
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
}
