package com.elione.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.elione.model.base.BaseModel;


/**
 * 
 * 投票的问题
 * 
 * @author 
 *
 */
@Entity
@Table(name="yh_vote")
@SequenceGenerator(name="sequenceGenerator", sequenceName="yh_vote_sequence")
public class Vote extends BaseModel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6288370599987702813L;
	
	private Long userID ;			//用户
	private Long questionID ;		//问题的编号
	private String answer ;			//如:A,B,C,D,E,F
	private Long useTime ;
	private int userNumber ;		//用户标识.cookie
	private Date createTime = new Date();

	
	public Long getUserID() {
		return userID;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public void setUserID(Long userID) {
		this.userID = userID;
	}
	public Long getQuestionID() {
		return questionID;
	}
	public void setQuestionID(Long questionID) {
		this.questionID = questionID;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public Long getUseTime() {
		return useTime;
	}
	public void setUseTime(Long useTime) {
		this.useTime = useTime;
	}
	public int getUserNumber() {
		return userNumber;
	}
	public void setUserNumber(int userNumber) {
		this.userNumber = userNumber;
	}
	
	
}
