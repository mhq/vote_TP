package com.elione.model;

import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.elione.model.base.BaseModel;


/**
 * 
 * 投票的问题
 * 
 * @author 
 *
 */
@Entity
@Table(name="yh_question")
@SequenceGenerator(name="sequenceGenerator", sequenceName="yh_question_sequence")
public class Question extends BaseModel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6288370599987702813L;
	
	private String questionNumber ;	//问题的标号,如: 第01题
	private String questionName ;	//如:洛阳是几朝古都?
	private String rightAnswers ; 	//如:A,B,C,D,E,F
	private int chooseCount ;	//选择的个数
	
	public String getQuestionNumber() {
		return questionNumber;
	}
	public void setQuestionNumber(String questionNumber) {
		this.questionNumber = questionNumber;
	}
	public String getQuestionName() {
		return questionName;
	}
	public void setQuestionName(String questionName) {
		this.questionName = questionName;
	}
	public String getRightAnswers() {
		return rightAnswers;
	}
	public void setRightAnswers(String rightAnswers) {
		this.rightAnswers = rightAnswers;
	}
	public int getChooseCount() {
		return chooseCount;
	}
	public void setChooseCount(int chooseCount) {
		this.chooseCount = chooseCount;
	}
	
}
