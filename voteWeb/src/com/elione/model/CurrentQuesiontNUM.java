package com.elione.model;

import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.elione.model.base.BaseModel;


/**
 * 
 * 投票的问题
 * 
 * @author 
 *
 */
@Entity
@Table(name="yh_current_quesiont_num")
@SequenceGenerator(name="sequenceGenerator", sequenceName="yh_current_quesiont_num_sequence")
public class CurrentQuesiontNUM extends BaseModel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6288370599987702813L;
	
	private Long questionID ;		//问题编号
	private boolean startVote ; 	//是否可以开始投票
	private Long startTime ;	//开始时间
	private int totalPeople ;	//总人数
	
	public Long getQuestionID() {
		return questionID;
	}

	public void setQuestionID(Long questionID) {
		this.questionID = questionID;
	}

	public boolean isStartVote() {
		return startVote;
	}
	
	public void setStartVote(boolean startVote) {
		this.startVote = startVote;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public int getTotalPeople() {
		return totalPeople;
	}

	public void setTotalPeople(int totalPeople) {
		this.totalPeople = totalPeople;
	}
	
	
	
}
