package com.elione.dao;

import java.util.List;

import com.elione.dao.base.BaseDao;
import com.elione.model.Answer;

public interface AnswerDao extends BaseDao<Answer, Long> {
	
	
	public List<Answer> getAnswersByQuestionID(Long questionID);
	
}
