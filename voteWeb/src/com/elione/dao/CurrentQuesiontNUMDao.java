package com.elione.dao;

import com.elione.dao.base.BaseDao;
import com.elione.model.CurrentQuesiontNUM;

public interface CurrentQuesiontNUMDao  extends BaseDao<CurrentQuesiontNUM, Long> {

	public CurrentQuesiontNUM getTop1OrderById();
}
