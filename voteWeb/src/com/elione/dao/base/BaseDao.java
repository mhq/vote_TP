package com.elione.dao.base;

import java.io.Serializable;
import java.util.List;


/**
 * 
 * @author mhq
 * @version 1.0 (26.06.2015)
 * 
 */
public interface BaseDao<T , ID extends Serializable> {

	/**
	 * 查找实体对象
	 * 
	 * @param id
	 *            ID
	 * @return 实体对象，若不存在则返回null
	 */
	T find(ID id);
	
	/**
	 * 持久化实体对象
	 * 
	 * @param entity
	 *            实体对象
	 */
	T persist(T entity);
	
	/**
	 * 合并实体对象
	 * 
	 * @param entity
	 *            实体对象
	 * @return 实体对象
	 */
	T merge(T entity);
	
	/**
	 * 移除实体对象
	 * 
	 * @param entity
	 *            实体对象
	 */
	void remove(T entity);
	
	/**
	 * 获取实体对象ID
	 * 
	 * @param entity
	 *            实体对象
	 * @return 实体对象ID
	 */
	ID getIdentifier(T entity);
	
	/**
	 * 分页获取信息
	 * 
	 * @param page
	 * @param rows
	 * @return
	 */
	List<T> getEntityListByPage( int page , int rows );
	
	/**
	 * 获取所有信息
	 * 
	 * @return
	 */
	List<T> findAll();
	
	/**
	 * 获取数据库行数
	 * 
	 * @return
	 */
	int getCount();
	
}
