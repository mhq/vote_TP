package com.elione.dao;

import com.elione.dao.base.BaseDao;
import com.elione.model.Question;

public interface QuestionDao extends BaseDao<Question, Long> {
	
	
}
