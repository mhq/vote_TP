package com.elione.dao;

import java.util.List;
import com.elione.dao.base.BaseDao;
import com.elione.model.UserInfo;
import com.elione.model.Vote;

public interface VoteDao extends BaseDao<Vote, Long> {

	public Vote save(Vote vote);
	public List<UserInfo> statistics();
	public Long getTime(Long userId);
	public int getVoteCountByQuestionID( Long questionID );
	public Vote getVoteByQuestionIDAndUserId( Long questionID , Long userID);
	public int getVoteCountByQuestionIDAndAnswer( Long questionID , String answer );
	public void deleteVotesByQuestionID(Long questionID);
}
