package com.elione.dao.impl;

import com.elione.dao.UserInfoDao;
import com.elione.dao.base.BaseDaoImpl;
import com.elione.model.UserInfo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.Query;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;


/**
 * 
 * �û�DAO
 * 
 * @author Genie
 * @version 1.0 (2019.09.28)
 *
 */
@Repository("userInfoDaoImpl")
public class UserInfoDaoImpl extends BaseDaoImpl<UserInfo, Long>implements UserInfoDao
{
	private Logger logger = LogManager.getLogger(UserInfoDaoImpl.class);

	public UserInfo getUserInfoByUserName(String name, String phone ){
		this.logger.debug(" get userinfo by username = " + name + " , phone = " + phone );
		Query query = this.entityManager.createQuery(" from UserInfo as user where user.name = ? and user.phone = ? ");
	    query.setParameter(1, name);
	    query.setParameter(2, phone);
	    List<?> list = query.getResultList();
	    if ( list.size() == 0 ) {
	      return null;
	    }
	    return (UserInfo)list.get(0);
	}
	
	public List<UserInfo> getUsers(){
	    Query query = this.entityManager.createQuery(" from UserInfo");
	    List<?> list = query.getResultList();
	    if ((list == null) || (list.size() == 0)) {
	      return null;
	    }
	    List<UserInfo> users = new ArrayList<UserInfo>();
	    for (Iterator<?> localIterator = list.iterator(); localIterator.hasNext(); ) { Object object = localIterator.next();
	      users.add((UserInfo)object);
	    }
	    return users;
	}

	  public boolean deleteUserByManagerType(Long managerType){
	    Query query = this.entityManager.createQuery(" delete from UserInfo as user where user.managerType = ? ");
	    query.setParameter(1, managerType);
	    int result = query.executeUpdate();
	    return result == 1;
	  }

	  public boolean selectByPhone(String phone){
	    Query query = this.entityManager.createQuery(" from UserInfo as user where user.phone = ?  ");
	    query.setParameter(1, phone);
	    List<?> list = query.getResultList();
	    if ((list == null) || (list.size() == 0)) {
	      return false;
	    }
	    return true;
	  }

	  public boolean selectByName(String name){
	    Query query = this.entityManager.createQuery(" from UserInfo as user where user.name = ?  ");
	    query.setParameter(1, name);
	    List<?> list = query.getResultList();
	    if ((list == null) || (list.size() == 0)) {
	      return false;
	    }
	    return true;
	   }

		@Override
		public UserInfo getUserInfoByUserName(String userName) {
			Query query = this.entityManager.createQuery(" from UserInfo as user where user.name = ? ");
		    query.setParameter(1, userName);
		    List<?> list = query.getResultList();
		    if ( list.size() == 0 ) {
		      return null;
		    }
		    return (UserInfo)list.get(0);
		}

		@Override
		public UserInfo getUserInfoByPhone(String phone) {
			Query query = this.entityManager.createQuery(" from UserInfo as user where user.phone = ? ");
		    query.setParameter(1, phone);
		    List<?> list = query.getResultList();
		    if ( list.size() == 0 ) {
		      return null;
		    }
		    return (UserInfo)list.get(0);
		}
		
}