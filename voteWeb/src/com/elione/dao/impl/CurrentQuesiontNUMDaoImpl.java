package com.elione.dao.impl;

import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.elione.dao.CurrentQuesiontNUMDao;
import com.elione.dao.base.BaseDaoImpl;
import com.elione.model.CurrentQuesiontNUM;

@Repository("currentQuesiontNUMDaoImpl")
public class CurrentQuesiontNUMDaoImpl extends
		BaseDaoImpl<CurrentQuesiontNUM, Long> implements CurrentQuesiontNUMDao {

	@Override
	public CurrentQuesiontNUM getTop1OrderById() {
		Query query = this.entityManager
				.createQuery(" from CurrentQuesiontNUM order by id DESC limit 0,1 ");
		List<?> list = query.getResultList();
		if ((list == null) || (list.size() == 0)) {
			return null;
		}
		return (CurrentQuesiontNUM) list.get(0);
	}

}
