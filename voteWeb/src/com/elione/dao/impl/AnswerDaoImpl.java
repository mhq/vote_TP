package com.elione.dao.impl;


import java.util.ArrayList;
import java.util.List;
import javax.persistence.Query;
import org.springframework.stereotype.Repository;
import com.elione.dao.AnswerDao;
import com.elione.dao.base.BaseDaoImpl;
import com.elione.model.Answer;


@Repository("answerDaoImpl")
public class AnswerDaoImpl extends BaseDaoImpl<Answer, Long> implements AnswerDao {

	@Override
	public List<Answer> getAnswersByQuestionID(Long questionID) {
		List<Answer> answers = new ArrayList<Answer>();
		Query query = this.entityManager
				.createQuery(" from Answer as model where model.questionID = ? ");
		query.setParameter(1, questionID);
		List<?> list = query.getResultList();
		if ((list == null) || (list.size() == 0)) {
			return null;
		} else {
			for (Object object : list) {
				answers.add( (Answer)object );
			}
		}
		return answers ;
	}
	
}
