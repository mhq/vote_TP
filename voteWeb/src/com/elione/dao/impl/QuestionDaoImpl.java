package com.elione.dao.impl;



import org.springframework.stereotype.Repository;
import com.elione.dao.QuestionDao;
import com.elione.dao.base.BaseDaoImpl;
import com.elione.model.Question;


@Repository("questionDaoImpl")
public class QuestionDaoImpl extends BaseDaoImpl<Question, Long> implements QuestionDao {
	

}
