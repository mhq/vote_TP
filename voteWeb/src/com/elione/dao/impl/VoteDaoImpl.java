package com.elione.dao.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.elione.dao.VoteDao;
import com.elione.dao.base.BaseDaoImpl;
import com.elione.model.UserInfo;
import com.elione.model.Vote;

@Repository("voteDaoImpl")
public class VoteDaoImpl  extends BaseDaoImpl<Vote, Long> implements VoteDao {

	@Override
	public Vote save(Vote vote) {
		super.entityManager.persist(vote);
		return vote;
	}
	
 

	public List<UserInfo> statistics(){
		Query query = super.entityManager.createNativeQuery("select a.* from yh_user_info a right join yh_vote b on b.userid = a.id left join yh_question c on c.id = b.questionid where b.answer = c.right_answers  ORDER BY a.id",UserInfo.class);
		List<UserInfo> users = new ArrayList<UserInfo>();
		List<?> list = query.getResultList();
		if ( list == null || list.size() == 0 ) {
			return null ;
		} else {
			for (Object object : list) {
				users.add( (UserInfo)object );
			}
			return users ;
		}
	}
	
	public Long getTime(Long userId){
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT TIMESTAMPDIFF ( SECOND,(");
		sql.append("select create_time from yh_vote where userid = " + userId + " order by create_time ASC limit 0,1");
		sql.append(" ) , (");
		sql.append("select create_time from yh_vote where userid =  " + userId + " order by create_time DESC limit 0,1");
		sql.append(") );");
		
		Query query = super.entityManager.createNativeQuery(sql.toString());		
		BigInteger result = (BigInteger )query.getSingleResult();
		return result.longValue();
	}

	@Override
	public int getVoteCountByQuestionID(Long questionID) {
		Query query = super.entityManager.createQuery("from Vote as model where model.questionID = ?" );
		query.setParameter(1, questionID);
		List<?> list = query.getResultList();
		if ( list == null ){
			return 0 ;
		} else {
			return list.size();
		}
	}

	@Override
	public Vote getVoteByQuestionIDAndUserId(Long questionID, Long userID) {
		Query query = super.entityManager.createQuery("from Vote as model where model.questionID = ? and model.userID = ? " );
		query.setParameter(1, questionID);
		query.setParameter(2, userID);
		List<?> list = query.getResultList();
		if ( list == null || list.size() == 0 ){
			return null ;
		} else {
			return (Vote) list.get( 0 );
		}
	}
	
	@Override
	public int getVoteCountByQuestionIDAndAnswer(Long questionID, String answer) {
		Query query = super.entityManager.createQuery("from Vote as model where model.questionID = ? and model.answer like ? " );
		query.setParameter(1, questionID);
		query.setParameter(2, "%" + answer + "%" );
		List<?> list = query.getResultList();
		if ( list == null ){
			return 0 ;
		} else {
			return list.size();
		}
	}
	
	@Override
	public void deleteVotesByQuestionID(Long questionID) {
		Query query = super.entityManager.createQuery("delete from Vote as model where model.questionID = ? ");
		query.setParameter(1, questionID);
		query.executeUpdate();
	}
	
	
}
