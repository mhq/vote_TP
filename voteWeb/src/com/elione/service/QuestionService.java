package com.elione.service;


import com.elione.model.Question;
import com.elione.service.base.BaseService;

public abstract interface QuestionService extends BaseService<Question, Long> {
	
}
