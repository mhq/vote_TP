package com.elione.service;

import java.util.List;

import com.elione.dao.model.VoteResult;
import com.elione.model.Vote;
import com.elione.service.base.BaseService;


public abstract interface VoteService extends BaseService<Vote, Long> {

	@Override
	public Vote save(Vote entity);
	
	public List<VoteResult> statistics();
	
	public int getVoteCountByQuestionID( Long questionID );
	
	public Vote getVoteByQuestionIDAndUserId( Long questionID , Long userID);
	
	public int getVoteCountByQuestionIDAndAnswer( Long questionID , String answer );
	
	public void deleteVotesByQuestionID(Long questionID);
	
	public List<Vote> getAll();
}
