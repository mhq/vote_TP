package com.elione.service.impl;


import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.elione.dao.QuestionDao;
import com.elione.model.Question;
import com.elione.service.QuestionService;
import com.elione.service.base.BaseServiceImpl;


@Service("questionServiceImpl")
public class QuestionServiceImpl extends BaseServiceImpl<Question, Long> implements
		QuestionService {
		
	  @Resource(name="questionDaoImpl")
	  public void setBasseDao(QuestionDao attributeDao) {
		 super.setBaseDao(attributeDao);
		 
	  }
	 
}
