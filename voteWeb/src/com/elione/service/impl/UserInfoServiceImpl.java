package com.elione.service.impl;

import com.elione.dao.UserInfoDao;
import com.elione.model.UserInfo;
import com.elione.service.UserInfoService;
import com.elione.service.base.BaseServiceImpl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

@Service("userInfoServiceImpl")
public class UserInfoServiceImpl extends BaseServiceImpl<UserInfo, Long>
  implements UserInfoService
{
  private Logger logger = LogManager.getLogger(UserInfoServiceImpl.class);

  @Resource(name="userInfoDaoImpl")
  private UserInfoDao userInfoDao;

  @Resource(name="userInfoDaoImpl")
  public void setBasseDao(UserInfoDao attributeDao) { super.setBaseDao(attributeDao); }

  
  public UserInfo getUserInfoByUserName(String userName, String phone)
  {
    this.logger.debug(" get userName = " + userName + " , phone = " + phone);
    return this.userInfoDao.getUserInfoByUserName(userName, phone);
  }

  public List<UserInfo> getUsers()
  {
    return this.userInfoDao.getUsers();
  }

  public boolean deleteUserByManagerType(Long managerType)
  {
    return this.userInfoDao.deleteUserByManagerType(managerType);
  }

  public boolean selectByPhone(String phone)
  {
    return this.userInfoDao.selectByPhone(phone);
  }

  public boolean selectByName(String name)
  {
    return this.userInfoDao.selectByName(name);
  }


	@Override
	public UserInfo getUserInfoByUserName(String userName) {
		return userInfoDao.getUserInfoByUserName(userName);
	}


	@Override
	public UserInfo getUserInfoByPhone(String phone) {
		return userInfoDao.getUserInfoByPhone(phone);
	}
}