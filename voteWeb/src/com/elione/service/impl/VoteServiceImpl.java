package com.elione.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.elione.controller.front.VoteAction;
import com.elione.dao.VoteDao;
import com.elione.dao.model.VoteResult;
import com.elione.model.UserInfo;
import com.elione.model.Vote;
import com.elione.service.VoteService;
import com.elione.service.base.BaseServiceImpl;

@Service("voteServiceImpl")
public class VoteServiceImpl extends BaseServiceImpl<Vote, Long> implements
		VoteService {

	private Logger logger = LogManager.getLogger(VoteAction.class);
	
	@Resource(name = "voteDaoImpl")
	private VoteDao voteDao;
	
	@Resource(name="voteDaoImpl")
	  public void setBasseDao(VoteDao attributeDao) {
		 super.setBaseDao(attributeDao);
	}
	
	@Override
	public Vote save(Vote entity) {
		return voteDao.save(entity);
	}

	public List<VoteResult> statistics() {
		
		List<UserInfo> user = voteDao.statistics();
		
		List<VoteResult> result = null ;
		
		try {
			
			result = doFraction(user);
			
			List<VoteResult> removeList = new ArrayList<VoteResult>();
			for(int i = 0;i<result.size();i ++){
				VoteResult temp = result.get(i);
				if(i >= 6){
					removeList.add(temp);
				}else{
					Long time = voteDao.getTime(temp.getId());
					temp.setTime(time);
				}
			}
			result.removeAll(removeList);
			Collections.sort(result,new Comparator<VoteResult>() {
				 public int compare(VoteResult arg0, VoteResult arg1) {
					 if(arg0.getFraction() == arg1.getFraction()){
		                return arg0.getTime().compareTo(arg1.getTime());
					 }else{
						 return arg1.getFraction().compareTo(arg0.getFraction());
					 }
		            }
			});
			
			logger.debug("-------------------------------------------------------------");
			
			for (VoteResult vr : result) {
				logger.debug(vr.getFraction() + " | " + vr.getHospital() + "|" + vr.getName() + " | "  + vr.getTime());
			}
			
			logger.debug("-------------------------------------------------------------");
		} catch (Exception e) {
			logger.error(" statistics =" + e , e );
		}
		
		return result;
	}
	
	private List<VoteResult> doFraction(List<UserInfo> user){
		Map<Long, VoteResult> mapResult = new HashMap<Long, VoteResult>();
		 for(UserInfo vr : user){
			 VoteResult temp = mapResult.get(vr.getId());
			 if(temp == null){
				 VoteResult vo = new VoteResult();
				 vo.setFraction(1);
				 vo.setName(vr.getName());
				 vo.setHospital(vr.getHospital());
				 vo.setId(vr.getId());
				 vo.setPhone(vr.getPhone());
				 mapResult.put(vr.getId(), vo);
			 }else{
				 temp.setFraction(temp.getFraction() + 1);
				 mapResult.put(vr.getId(), temp);
			 }
		 }
		 List<VoteResult> result = new ArrayList<VoteResult>(mapResult.values());
		 Collections.sort(result,new Comparator<VoteResult>() {
			 public int compare(VoteResult arg0, VoteResult arg1) {
	                return arg1.getFraction().compareTo(arg0.getFraction());
	            }
		});
		 return  result;
	}

	@Override
	public int getVoteCountByQuestionID(Long questionID) {
		return voteDao.getVoteCountByQuestionID(questionID);
	}

	@Override
	public Vote getVoteByQuestionIDAndUserId(Long questionID, Long userID) {
		return voteDao.getVoteByQuestionIDAndUserId(questionID, userID);
	}

	@Override
	public int getVoteCountByQuestionIDAndAnswer(Long questionID, String answer) {
		return voteDao.getVoteCountByQuestionIDAndAnswer(questionID, answer);
	}

	@Override
	public void deleteVotesByQuestionID(Long questionID) {
		voteDao.deleteVotesByQuestionID(questionID);
	}

	public List<Vote> getAll() {
		return voteDao.findAll();
	}
	
	
}
