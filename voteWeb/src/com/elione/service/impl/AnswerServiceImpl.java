package com.elione.service.impl;


import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.elione.dao.AnswerDao;
import com.elione.model.Answer;
import com.elione.service.AnswerService;
import com.elione.service.base.BaseServiceImpl;

@Service("answerServiceImpl")
public class AnswerServiceImpl extends BaseServiceImpl<Answer, Long> implements
		AnswerService {
	
	@Resource(name = "answerDaoImpl")
	private AnswerDao answerDao;
	
	@Resource(name = "answerDaoImpl")
    public void setBasseDao(AnswerDao answerDao) {
	   super.setBaseDao(answerDao);
    }

	@Override
	public List<Answer> getAnswersByQuestionID(Long questionID) {
		return answerDao.getAnswersByQuestionID(questionID);
	}
	
}
