package com.elione.service.impl;

import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.elione.dao.CurrentQuesiontNUMDao;
import com.elione.model.CurrentQuesiontNUM;
import com.elione.service.CurrentQuesiontNUMService;
import com.elione.service.base.BaseServiceImpl;

@Service("currentQuesiontNUMServiceImpl")
public class CurrentQuesiontNUMServiceImpl extends BaseServiceImpl<CurrentQuesiontNUM, Long> implements CurrentQuesiontNUMService{

	@Resource(name = "currentQuesiontNUMDaoImpl")
	private CurrentQuesiontNUMDao currentQuesiontNUMDao;
	
	@Resource(name = "currentQuesiontNUMDaoImpl")
    public void setBasseDao(CurrentQuesiontNUMDao attributeDao) {
	   super.setBaseDao(attributeDao);
    }
	
	@Override
	public CurrentQuesiontNUM getTop1OrderById() {
		return currentQuesiontNUMDao.getTop1OrderById();
	}

}
