package com.elione.service;

import com.elione.model.CurrentQuesiontNUM;
import com.elione.service.base.BaseService;

public interface CurrentQuesiontNUMService extends BaseService<CurrentQuesiontNUM, Long> {

	public CurrentQuesiontNUM getTop1OrderById();
}
