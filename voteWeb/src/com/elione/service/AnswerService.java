package com.elione.service;


import java.util.List;
import com.elione.model.Answer;
import com.elione.service.base.BaseService;

public abstract interface AnswerService extends BaseService<Answer, Long> {
	
	public List<Answer> getAnswersByQuestionID(Long questionID);
	
}
