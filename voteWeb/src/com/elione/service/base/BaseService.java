package com.elione.service.base;

import java.io.Serializable;
import java.util.List;

public interface BaseService <T, ID extends Serializable> {
	
	
	/**
	 * 根据ID查询对象
	 * 
	 * @param id
	 * @return
	 */
	T find(ID id);
	
	/**
	 * save entity
	 * 
	 * @param entity
	 */
	T save(T entity);

	/**
	 * update entity
	 * 
	 * @param entity
	 * @return
	 */
	T update(T entity);
	
	/**
	 * 根据ID删除
	 * 
	 * @param id
	 */
	void delete(ID id);
	
	/**
	 * 根据对象删除
	 * 
	 * @param entity
	 */
	void delete(T entity);
	
	/**
	 * 分页获取信息
	 * 
	 * @param page
	 * @param rows
	 * @return
	 */
	List<T> getEntityListByPage( int page , int rows );
	
	/**
	 * 获取所有信息
	 * 
	 * @return
	 */
	List<T> findAll();
	
	/**
	 * 获取数据库行数
	 * 
	 * @return
	 */
	int getCount();
}
