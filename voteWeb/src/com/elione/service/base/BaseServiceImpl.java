package com.elione.service.base;

import java.io.Serializable;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;
import com.elione.dao.base.BaseDao;


@Transactional
public abstract class BaseServiceImpl<T, ID extends Serializable> implements BaseService<T, ID> {

	private Logger logger = LogManager.getLogger( BaseServiceImpl.class );
	
	/** baseDao */
	private BaseDao<T, ID> baseDao ;
	
	public void setBaseDao(BaseDao<T, ID> baseDao) {
		this.baseDao = baseDao;
	}
	
	@Transactional(readOnly = true)
	public T find(ID id) {
		return baseDao.find(id);
	}
	
	@Transactional(readOnly = true)
	public List<T> findAll() {
		return baseDao.findAll();
	}
	
	@Transactional
	public T save(T entity) {
		return baseDao.persist(entity);
	}

	@Transactional
	public T update(T entity) {
		return baseDao.merge(entity);
	}
	
	@Transactional
	public void delete(ID id) {
		delete(baseDao.find(id));
	}
	
	@Transactional
	public void delete(T entity) {
		baseDao.remove(entity);
	}
	
	@Override
	public List<T> getEntityListByPage(int page, int rows) {
		logger.debug(" page =" + page + " , rows =" + rows );
		return baseDao.getEntityListByPage(page, rows);
	}

	@Override
	public int getCount() {
		return baseDao.getCount() ;
	}
	
	
	
}
