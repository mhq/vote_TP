package com.elione.controller.front;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.Cookie;

import net.sf.json.JSONArray;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import com.elione.controller.base.AbstractAction;
import com.elione.dao.model.VoteResult;
import com.elione.model.Answer;
import com.elione.model.CurrentQuesiontNUM;
import com.elione.model.Question;
import com.elione.model.UserInfo;
import com.elione.model.Vote;
import com.elione.service.AnswerService;
import com.elione.service.CurrentQuesiontNUMService;
import com.elione.service.QuestionService;
import com.elione.service.UserInfoService;
import com.elione.service.VoteService;
import com.elione.tools.ApplicationContextProvider;

public class VoteAction extends AbstractAction {

	
	public static final String NO_DATA = "NO_DATA"; 
	public static int cookieNum = 10000 ;
	private static final long serialVersionUID = -1771852900768732799L;
	
	private Logger logger = LogManager.getLogger(VoteAction.class);
	private String answer;
	
	private long currentQuestionID ;  //当前的问题编号
	private long updateQuestionId ;  //修改当前问题的编号
	
	private long currentQuestionID_phone;//手机当前问题
	
	UserInfoService userInfoService = (UserInfoService)ApplicationContextProvider.getApplicationContext().getBean("userInfoServiceImpl");
	AnswerService answerService = (AnswerService)ApplicationContextProvider.getApplicationContext().getBean("answerServiceImpl");
	VoteService voteServiceImpl = (VoteService)ApplicationContextProvider.getApplicationContext().getBean("voteServiceImpl");
	CurrentQuesiontNUMService currentQuesiontNUMService = (CurrentQuesiontNUMService)ApplicationContextProvider.getApplicationContext().getBean("currentQuesiontNUMServiceImpl");
	QuestionService questionService = (QuestionService)ApplicationContextProvider.getApplicationContext().getBean("questionServiceImpl");
	
	
	/**
	 * 获取所有投票人的手机号
	 */
	public void vote_user_phones(){
		List<UserInfo> users=  userInfoService.getUsers();
		StringBuffer phones = new StringBuffer();
		if(null !=users&&users.size()>0){
			for(UserInfo v:users){
				phones.append(v.getPhone()+",");
			}
		}
		try {
			getResponse().getWriter().print(phones.length()>0?phones.substring(0, phones.length()-1):"");
		} catch (IOException e) {
			logger.warn(" error =" + e , e );
		}
	}
	
	
	
	
	/**
	 * 手机端 切换轮数是否可以投票
	 * @throws IOException
	 */
	public void phone_can_vote()throws IOException {
		CurrentQuesiontNUM num = currentQuesiontNUMService.getTop1OrderById();
		Long quetionsid = num.getQuestionID();
		String can_vote_st = "0001";//不可以投票
		if(currentQuestionID_phone==quetionsid&&num.isStartVote()){//可以投票
			can_vote_st =    "0000";
		}
		getResponse().getWriter().print(can_vote_st);
	}
	
	
	
	/**
	 * 手机端  获取有多少轮投票
	 */
	public void question_list(){
		//获取多少问题
		List<Question> questions =  questionService.findAll();
		JSONArray jsonList = JSONArray.fromObject(questions);
		PrintWriter out = null ;
		try {
			out = super.getResponse().getWriter();
		} catch (IOException e) {
			logger.warn(" error =" + e , e );
		}
		out.print( jsonList.toString() );
	}
	
	/**
	 * 手机端  获取每一轮选项
	 */
	public void ansers_init(){
		List<Answer> answers = answerService.getAnswersByQuestionID( currentQuestionID_phone );
		JSONArray jsonList = JSONArray.fromObject(answers);
		PrintWriter out = null ;
		try {
			out = super.getResponse().getWriter();
		} catch (IOException e) {
			logger.warn(" error =" + e , e );
		}
		out.print( jsonList.toString() );
	}
	
	
	
	
	/**
	 * 投票的方法
	 * @throws IOException
	 */
	public void vote() throws IOException {
		
		String userNumber = null ;
		this.logger.debug("userNumber =" + userNumber + " , vote answer:" + answer);
		boolean isExit = false ; 
		//获取cookie
		Cookie[] cookies = super.getRequest().getCookies();  
		// 然后迭代之  
		if ( cookies != null && cookies.length > 0 ) { //如果没有设置过Cookie会返回null  
		    for (Cookie cookie : cookies) {
		    	String name = cookie.getName();
		    	if (name.equals("userNumber")) {
		    		isExit = true ;
		    		userNumber = cookie.getValue();
		    	}
		    }  
		}
		
		logger.debug("是否存在当前 cookie 用户  [" + userNumber + "] => " + isExit );
		
		if ( !isExit ) {
			//如果不存在cookie,就重新登录
			getResponse().getWriter().print("000001");
		} else {
			logger.debug(" 生成的 cookie 用户  = " + userNumber );
			CurrentQuesiontNUM num = currentQuesiontNUMService.getTop1OrderById();
			boolean isStartVote = num.isStartVote();
			if ( isStartVote ) {
				logger.debug("当前状态允许投票!");
				if(num != null){
					UserInfo userInfo = userInfoService.getUserInfoByPhone(userNumber);
					//如果userInfo不存在,说明cookie存在,但是数据库对应的手机号已经被清除
					if ( userInfo == null ) {
						logger.info("不存在此用户");
						getResponse().getWriter().print("000007");
						return ;
					}
					
					Vote refVote = voteServiceImpl.getVoteByQuestionIDAndUserId( num.getQuestionID() , userInfo.getId() );
					//Vote refVote = null ;
					//如果数据库不存在信息,就保存
					//if ( refVote == null ) {
					Vote vote = new Vote();
					//不存在的处理
					if ( refVote == null ) {
						logger.debug("本题,用户没有投票!");
						
						vote.setUserID( userInfo.getId() );
						vote.setQuestionID(num.getQuestionID());
					} else {
						logger.debug("用户已经投票,更新票!");
						vote = refVote ;
					}
					vote.setAnswer(answer);
					//vote.setUserNumber( Integer.parseInt( userNumber ) );
					Long currentTime = System.currentTimeMillis();
					Long useTime = currentTime - num.getStartTime();
					vote.setUseTime(useTime);
					Question question = questionService.find( num.getQuestionID() );
					int chooseCount = question.getChooseCount();
					int answerLength = answer.length();
					logger.debug(" answer value =[" + answer + "]" + answerLength + "===" +  chooseCount );
					//添加判断
					if (! answer.equals("") && !answer.equals(".") && answerLength <= chooseCount  ) {
						this.voteServiceImpl.update( vote );
						//vote = this.voteServiceImpl.update( vote );
						/*if (vote == null) {
							getResponse().getWriter().print("0001");
						} else {
							getResponse().getWriter().print("0000");
						}*/
						
						logger.debug("-------------- answer value --------------" + answer );
						
						//投票成功
						if ( answer.equals("1") ) {
							getResponse().getWriter().print("0000");
						} else {
							getResponse().getWriter().print("00001");
						}
						
					} else {
						//投票失败 , 数据有误
						getResponse().getWriter().print("0005");
					}
					/*} else {
						//如果数据库有插入当前投票,什么也不做
						getResponse().getWriter().print("0004");
					}*/
				}/*else{
					logger.info("获取问题ID失败");
					getResponse().getWriter().print("0002");
				}*/
			} else {
				logger.debug("当前状态不允许投票!");
				getResponse().getWriter().print("0003");
			}
		}
		
	}
	
	
	/**
	 * 
	 * 初始化投票的状态
	 * 
	 * 设置投票的编号为1
	 * 
	 */
	public void initVoteState(){
		CurrentQuesiontNUM questionNUM = currentQuesiontNUMService.getTop1OrderById();
		if ( questionNUM == null ) {
			questionNUM = new CurrentQuesiontNUM();
		}
		questionNUM.setQuestionID( 1l );	//设置投票编号为1,默认的
		questionNUM.setStartVote( false );	//默认设置为不能投票
		currentQuesiontNUMService.update(questionNUM);
	}
	
	/**
	 * 
	 * 更新当前题目的编号
	 * 1 : 更新编号
	 * 2 : 设置为不能投票
	 * 
	 */
	public void updateCurrentQuestionID(){
		List<Question> questions = questionService.findAll();
		int questionCount = questions.size();
		logger.debug(" VoteCount =" + questionCount + " , updateQuestionId =" + updateQuestionId );
		if ( updateQuestionId > 0 && updateQuestionId <= questionCount ) {
			CurrentQuesiontNUM questionNUM = currentQuesiontNUMService.getTop1OrderById();
			questionNUM.setQuestionID( updateQuestionId );
			questionNUM.setStartVote( false );		//更新题目编号的时候,同时更新投票的状态,设置为关闭状态
			currentQuesiontNUMService.update(questionNUM);
		} else {
		}
		CurrentQuesiontNUM questionInfo = currentQuesiontNUMService.getTop1OrderById();
		long currentID = questionInfo.getQuestionID();
		PrintWriter out = null ;
		try {
			out = super.getResponse().getWriter();
		} catch (IOException e) {
			logger.warn(" error =" + e , e );
		}
		out.print( currentID );
	}
	
	/**
	 * 
	 * 获取当前问题的正确答案
	 * 
	 */
	public void getCurrentVoteRightAnswer(){
		Question question = questionService.find(currentQuestionID);
		String rightAnswer = question.getRightAnswers();
		PrintWriter out = null ;
		try {
			out = super.getResponse().getWriter();
		} catch (IOException e) {
			logger.warn(" error =" + e , e );
		}
		logger.debug("问题编号 =" + currentQuestionID + " , 正确答案 =" + rightAnswer );
		out.print( rightAnswer );
	}
	
	
	/**
	 * 
	 * 打开投票功能
	 * 
	 */
	public void openVoteFuc(){
		//当前问题的状态
		CurrentQuesiontNUM questionInfo = currentQuesiontNUMService.getTop1OrderById();
		questionInfo.setStartVote( true );		//开启投票
		Long currentTime = System.currentTimeMillis();
		questionInfo.setStartTime( currentTime );
		
		currentQuesiontNUMService.update(questionInfo);
	}
	
	/**
	 * 
	 * 关闭投票功能
	 * 
	 */
	public void closeVoteFuc(){
		//当前问题的状态
		CurrentQuesiontNUM questionInfo = currentQuesiontNUMService.getTop1OrderById();
		questionInfo.setStartVote( false );		//开启投票
		currentQuesiontNUMService.update(questionInfo);
	}
	
	/**
	 * 
	 * 获取当前问题的投票数量
	 * 
	 */
	public void getVoteCount(){
		int voteCount = voteServiceImpl.getVoteCountByQuestionID( currentQuestionID );
		try {
	        getResponse().getWriter().print( voteCount );
	    } catch (IOException e) {
	        this.logger.warn(" namePhoneCheck error =" + e, e);
	    }
	}
	
	/**
	 * 
	 * 获取当前投票答案的所有编号
	 * 
	 */
	public void getVoteAnswerIds(){
		List<Answer> answers = answerService.getAnswersByQuestionID( currentQuestionID );
		List<String> ids = new ArrayList<String>();
		for (Answer answer : answers) {
			ids.add( answer.getAnswerValue() );
		}
		JSONArray jsonList = JSONArray.fromObject( ids );
		PrintWriter out = null ;
		try {
			out = super.getResponse().getWriter();
		} catch (IOException e) {
			logger.warn(" error =" + e , e );
		}
		out.print( jsonList.toString() );
	}
	
	/**
	 * 
	 * 获取当前投票每个答案的正确数量
	 * 
	 */
	public void getVoteAnswerRightCount(){
		List<Answer> answers = answerService.getAnswersByQuestionID( currentQuestionID );
		List<Integer> values = new ArrayList<Integer>();
		for (Answer answer : answers) {
			int count = voteServiceImpl.getVoteCountByQuestionIDAndAnswer( currentQuestionID , answer.getAnswerValue() );
			values.add( count );
		}
		JSONArray jsonList = JSONArray.fromObject( values );
		PrintWriter out = null ;
		try {
			out = super.getResponse().getWriter();
		} catch (IOException e) {
			logger.warn(" error =" + e , e );
		}
		out.print( jsonList.toString() );
	}
	
	/**
	 * 
	 * 获取当前投票的信息
	 * 
	 * 1 : 当前问题的答案编号 , 如 : A , B , C , D 
	 * 2 : 答案对应的投票数量 , 如 : A : 10 , B : 20 , C : 15
	 * 3 : 总投票数量
	 * 
	 * 此处暂时不需要优化
	 * 
	 * 总体查询时间只有 50 MS 左右.  查询时间为2秒
	 * 
	 */
	public void getCurrentVoteInfo(){
		
		long startTime = System.currentTimeMillis();
		
		List<Question> questionList = questionService.findAll();
		
		//当前问题的状态
		CurrentQuesiontNUM questionInfo = currentQuesiontNUMService.getTop1OrderById();
		int totalPeople = questionInfo.getTotalPeople();
		
		List<String> idsList = new ArrayList<String>();
		List<Integer> valuesList = new ArrayList<Integer>();
		
		for (Question question : questionList) {
			//idsList.add( question.getQuestionNumber() );
			idsList.add( "" );
			int count = voteServiceImpl.getVoteCountByQuestionIDAndAnswer( question.getId() , "1" );
			valuesList.add( count );
		}
		
		JSONArray ids = JSONArray.fromObject( idsList );
		JSONArray values = JSONArray.fromObject( valuesList );
		int voteCount = voteServiceImpl.getVoteCountByQuestionID( currentQuestionID );
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("ids", ids);//选项
		jsonObject.put("values", values);//投票数量
		jsonObject.put("voteCount", voteCount);//该轮总的投票数量
		jsonObject.put("totalPeople", totalPeople);//投票总人数
		
		logger.debug( " ------- GetCurrentVoteInfo totol time =" + ( System.currentTimeMillis() - startTime ) );  
		
		PrintWriter out = null ;
		try {
			out = super.getResponse().getWriter();
		} catch (IOException e) {
			logger.warn(" error =" + e , e );
		}
		out.print( jsonObject.toString() );
	}
	
	/**
	 * 
	 * 删除当前题目投票数据
	 * 
	 * 删除完成后,同时设置为不能投票
	 * 
	 */
	public void deleteCurrentQuestionVoteData(){
		logger.debug("删除当前题目投票数据,编号为=" + currentQuestionID );
		voteServiceImpl.deleteVotesByQuestionID(currentQuestionID);
		PrintWriter out = null ;
		try {
			out = super.getResponse().getWriter();
		} catch (IOException e) {
			logger.warn(" error =" + e , e );
		}
		CurrentQuesiontNUM questionNUM = currentQuesiontNUMService.getTop1OrderById();
		questionNUM.setStartVote( false );		//更新题目编号的时候,同时更新投票的状态,设置为关闭状态
		currentQuesiontNUMService.update(questionNUM);
		
		out.print( "删除当前投票数据成功!" );
	}
	
	
	/**
	 * 
	 * 投票结果
	 * 
	 * @return
	 */
	public void voteResult(){
		
		List<VoteResult> voteResult = voteServiceImpl.statistics();
		
		this.getRequest().setAttribute("voteResult", voteResult);
		
		logger.debug("查看投票结果 !");
		
		JSONObject jsonObjct = new JSONObject();
		jsonObjct.put("voteResult", voteResult);
		try {
			this.getResponse().getWriter().print( jsonObjct.toString() );
		} catch (IOException e) {
			logger.error(" voteResult error =" + e , e );
		}
	}
	
	
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public static int getCookieNum() {
		return cookieNum;
	}

	public static void setCookieNum(int cookieNum) {
		VoteAction.cookieNum = cookieNum;
	}

	public Long getCurrentQuestionID() {
		return currentQuestionID;
	}

	public void setCurrentQuestionID(Long currentQuestionID) {
		this.currentQuestionID = currentQuestionID;
	}

	public long getUpdateQuestionId() {
		return updateQuestionId;
	}
	
	public void setUpdateQuestionId(long updateQuestionId) {
		this.updateQuestionId = updateQuestionId;
	}




	public long getCurrentQuestionID_phone() {
		return currentQuestionID_phone;
	}




	public void setCurrentQuestionID_phone(long currentQuestionIDPhone) {
		currentQuestionID_phone = currentQuestionIDPhone;
	}
	
	
	
}
