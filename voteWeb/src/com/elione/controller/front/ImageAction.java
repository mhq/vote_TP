package com.elione.controller.front;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;

import org.apache.struts2.ServletActionContext;
import org.bouncycastle.asn1.ocsp.Request;

import com.elione.controller.base.AbstractAction;

public class ImageAction extends AbstractAction{
	
	private File file;
	private String fileFileName;
	private String type;
	private String imgname;
	
	
	public void upload() throws Exception{
		String msg = "";
		String savepath = "/images/phone/images/";
		if(null !=type){
			if(type.equals("1")){
				savepath = "/images/phone/images/";
			}
			if(type.equals("2")){
				savepath = "/bgImg/";
			}
		}else{
			msg = "类型必填";
		}
		if(null !=imgname&&!imgname.trim().equals("")){
			if(null !=file){
				String flag = this.saveattach(imgname,savepath);
				if(flag.equals("0")){
					msg = "上传失败，图片类型有误";
				}else{
					msg = "上传成功";
				}
				 
			}else{
				msg = "文件不能为空";
			}
		}else{
			msg = "图片名称不能为空";
		}
		this.getRequest().setAttribute("msg", msg);
		this.getRequest().getRequestDispatcher("upload.jsp").forward(this.getRequest(), this.getResponse());
		
	}
	
	
	
	public String saveattach(String imgname,String savepath) {
		try {
			BufferedImage bi = ImageIO.read(file);
			if (bi == null) {
				return "0";
			}
		} catch (IOException e) {
			return "0";
		}
		String realpath = ServletActionContext.getServletContext().getRealPath(savepath);
		if (!new File(realpath).exists()) {
			new File(realpath).mkdir();
		}
		String[] typechoose = fileFileName.split("\\.");
		int ichoose = typechoose.length;
		String type = ichoose > 1 ? typechoose[ichoose - 1] : "";
		if (type.toLowerCase().equals("jpg")
				|| type.toLowerCase().equals("gif")
				|| type.toLowerCase().equals("jpeg")
				|| type.toLowerCase().equals("png")) {
			String newfilname = imgname + "." + type;
			String path = realpath + "/" + newfilname;
			saveFile(path, file);
			return newfilname;
		} else {
			return "0";
		}
	}
	
	public String saveattach() {
		try {
			BufferedImage bi = ImageIO.read(file);
			if (bi == null) {
				return "0";
			}
		} catch (IOException e) {
			return "0";
		}
		String realpath = ServletActionContext.getServletContext().getRealPath(
				"/phone/images/");
		if (!new File(realpath).exists()) {
			new File(realpath).mkdir();
		}
		String[] typechoose = fileFileName.split("\\.");
		int ichoose = typechoose.length;
		String type = ichoose > 1 ? typechoose[ichoose - 1] : "";
		if (type.toLowerCase().equals("jpg")
				|| type.toLowerCase().equals("gif")
				|| type.toLowerCase().equals("jpeg")
				|| type.toLowerCase().equals("png")) {
			SimpleDateFormat smat = new SimpleDateFormat("yyyyMMddHHmmss");
			String newfilname = smat.format(new Date()) + "." + type;
			String path = realpath + "/" + newfilname;
			saveFile(path, file);
			return newfilname;
		} else {
			return "0";
		}
	}
	
	
	
	
	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getImgname() {
		return imgname;
	}


	public void setImgname(String imgname) {
		this.imgname = imgname;
	}


	public static void saveFile(String savePath, File upload) {
		try {
			InputStream in = null;
			OutputStream out = null;
			in = new FileInputStream(upload);
			out = new FileOutputStream(savePath);
			int readed = 0;
			byte[] buffer = new byte[1024];
			while ((readed = in.read(buffer, 0, 1024)) != -1) {
				out.write(buffer, 0, readed);
			}
			out.flush();
			out.close();
			in.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public String getFileFileName() {
		return fileFileName;
	}

	public void setFileFileName(String fileFileName) {
		this.fileFileName = fileFileName;
	}

	@Override
	public void validate() {
	}
}
