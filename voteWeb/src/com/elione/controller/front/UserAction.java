package com.elione.controller.front;

import com.elione.controller.base.AbstractAction;
import com.elione.model.UserInfo;
import com.elione.service.UserInfoService;
import com.elione.tools.ApplicationContextProvider;
import com.elione.tools.SessionHelper;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import javax.servlet.http.Cookie;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class UserAction extends AbstractAction{
  
	  private static final long serialVersionUID = -7961486211733121674L;
	  
	  private Logger logger = LogManager.getLogger(UserAction.class);
	  
	  private String name ;	//姓名
	  private String phone ; //电话
	  private String hospital ;	//医院
	  
	  private String section;
	  
	  
	  UserInfoService userInfoService = (UserInfoService)ApplicationContextProvider.getApplicationContext().getBean("userInfoServiceImpl");
	
	  final int maxAge = 86400 ;  //one day
  
  
	  /**
	   * 
	   * 进入系统
	   * 
	   * 判断cookie是否存在
	   *  存在 : 进入聊天
	   *  不存在 : 进入登录
	   * 
	   * @return
	   */
	  public String access(){
		
		boolean isExit = false ; 
		  
		String userNumber = null ;
		
		//获取cookie
		Cookie[] cookies = super.getRequest().getCookies();  
		// 然后迭代之  
		if ( cookies != null && cookies.length > 0 ) { //如果没有设置过Cookie会返回null  
		    for (Cookie cookie : cookies) {
		    	String name = cookie.getName();
		    	if (name.equals("userNumber")) {
		    		isExit = true ;
		    		userNumber = cookie.getValue();
		    	}
		    }  
		}
		
		logger.debug("=====[" + userNumber + "] =====" + isExit  );
		
		if ( isExit ) {
			//存在,进入。。。。
			return SUCCESS ;
		} else {
			//不存在,进入注册，登录
			return LOGIN ;
		}
		
	  }
  
	  
	  /**
	   * 
	   * 登录注册一体 
	   * 
	   * 登录注册结束后,创建cookie,保存一段时间内不需要再次登录
	   * 
	   * @return
	   */
	  public String loginAndRegister(){
		    this.logger.debug("Login userName =" + this.name + " , section =" + this.section);
		    UserInfo userInfo = this.userInfoService.getUserInfoByUserName(this.name, this.section);
		    if (userInfo == null){
		        //数据库中没有任何用户信息
		    	userInfo = new UserInfo();
		        userInfo.setName(this.name);//名称
		        userInfo.setPhone( this.phone );//手机号
		        this.userInfoService.save(userInfo);
		        String coName = "" ;
			    String coSection = "" ;
				try {
					coName = URLEncoder.encode(userInfo.getName() + "" ,"utf-8");
					coSection = URLEncoder.encode(userInfo.getPhone() + "" ,"utf-8");
					logger.debug("Save cookie info , coName =" + coName + " , coSection =" + coSection );
				} catch (UnsupportedEncodingException e) {
					logger.warn(" encod error =" + e , e );
				}
			    Cookie cookieName = new Cookie("coName",coName );
			    Cookie cookiePhone = new Cookie("coSection", coSection );
			    Cookie userNumber = new Cookie("userNumber", coSection);
			    
			    cookieName.setMaxAge( maxAge );
			    cookiePhone.setMaxAge( maxAge );
			    userNumber.setMaxAge( maxAge );
			    cookieName.setPath("/");
			    cookiePhone.setPath("/");
			    userNumber.setPath("/");
			    super.getResponse().addCookie( cookieName );
			    super.getResponse().addCookie( cookiePhone );
			    super.getResponse().addCookie( userNumber );
		    }
		    //保存session信息
	//	    super.getSession().setAttribute(SessionHelper.sessionKey, userInfo);
		    //保存cookie信息
		    this.logger.debug("登录成功 : " + userInfo);
		    return "success";
	  }
  
  
	  /**
	   * 进入登录
	   * 
	   * 如果登录过了.直接进入投票
	   */
	  public void enterInfoLogin()throws IOException{
		  boolean isExit = false ;
			//获取cookie 
			Cookie[] cookies = super.getRequest().getCookies(); 
			String coName = "" ;
			String coValue = "" ;
			// 然后迭代之  
			if ( cookies != null && cookies.length > 0 ) { //如果没有设置过Cookie会返回null  
			    for (Cookie cookie : cookies) {
			    	coName = cookie.getName();
			    	if ( coName.equals("userNumber") ) {
			    		coValue = cookie.getValue();
			    		isExit = true ;
			    		break ;
			    	}
			    }
			}
			logger.debug("=====[" + coName + "]=====" + isExit + "==== coValue =" + coValue );
			if ( isExit ) {
				//存在,进入登录系统
				getResponse().getWriter().print("00");
			} else {
				//不存在,进入登录
				getResponse().getWriter().print("01");
			}
	  }
  
  
	  /**
	   *
	   * 登录操作
	   * 
	   * - 判断是否存在当前用户信息
	   * 
	   * 
	   * @return
	   */
	  public void login(){
		    
		  this.logger.debug("Login userName =" + this.name + " , phone =" + this.phone);
		    
		  UserInfo userInfo = this.userInfoService.getUserInfoByUserName(this.name, this.phone);
		    
		  if (userInfo == null){
			  //数据库中没有任何用户信息
			  try {
				  logger.debug("此用户信息不存在 !");
				  getResponse().getWriter().print("10001");
			  } catch (IOException e) {
				  logger.error("login print error =" + e , e );
			  }
		  }else{
		      //保存session信息
			  super.getSession().setAttribute(SessionHelper.sessionKey, userInfo);
			  this.logger.debug("账号信息存在 , 登录成功 : " + userInfo);
			  
			  //保存cookie信息
			  String coName = "" ;
			  String coPhone = "" ;
			  try {
				  coName = URLEncoder.encode( userInfo.getName() + "" ,"utf-8");
				  coPhone = URLEncoder.encode( userInfo.getPhone() + "" ,"utf-8");
				  logger.debug("Save cookie info , coName =" + coName + " , coSection =" + coPhone );
			  } catch (UnsupportedEncodingException e) {
				  logger.warn(" encod error =" + e , e );
			  }
			  Cookie cookieName = new Cookie("coName",coName );
			  Cookie cookiePhone = new Cookie("coPhone", coPhone);
			  Cookie userNumber = new Cookie("userNumber", coPhone);
			  	
			  cookieName.setMaxAge( maxAge );
			  cookiePhone.setMaxAge( maxAge );
			  userNumber.setMaxAge( maxAge );
			  cookieName.setPath("/");
			  cookiePhone.setPath("/");
			  userNumber.setPath("/");
			  super.getResponse().addCookie( cookieName );
			  super.getResponse().addCookie( cookiePhone );
			  super.getResponse().addCookie( userNumber );
			  
			  this.logger.debug("Session , Cookie 设置成功 ! ");
			  
			  try {
				  getResponse().getWriter().print("10000");
			  } catch (IOException e) {
				  logger.warn("login print error =" + e , e );
			  }
			  
		   }    
	  }
	  
	  public void namePhoneCheck(){
	    /*UserInfo userInfo = this.userInfoService.getUserInfoByUserName(this.name, this.phone);
	    if (userInfo == null)
	      try {
	        getResponse().getWriter().print("姓名和手机号不匹配,请核实信息!");
	      } catch (IOException e) {
	        this.logger.warn(" namePhoneCheck error =" + e, e);
	      }*/
	  }
  
  
	  /**
	   * 
	   * 注册
	   * 
	   * @return
	   */
	  public String register()
	  {
	    this.logger.debug("Register userName =" + this.name + " , phone =" + this.phone + " , hospital =" + this.hospital );
	    
	    UserInfo userResult = userInfoService.getUserInfoByUserName( this.name , this.phone );
	    
	    UserInfo userInfo = new UserInfo();
	    
	    //不存在就添加 (通常,注册是只保存信息而不更新信息的,但是系统无缘无故会有重复的数据,具体问题暂时没有找到,所以在保存接口这里做一层过滤)
	    if ( userResult == null ) {
		    userInfo.setName(this.name);
		    userInfo.setPhone(this.phone);
		    userInfo.setHospital(this.hospital);
		    this.userInfoService.save(userInfo);
	    } else {
	    	//存在就更新
	    	userInfo = userResult ;
	    	userInfo.setHospital(this.hospital);
		    this.userInfoService.update( userInfo );
	    }
	    
	    super.getSession().setAttribute(SessionHelper.sessionKey, userInfo);
	    
	    this.logger.debug("账号信息存在 , 登录成功 : " + userInfo);
		
	    //保存cookie信息
	    String coName = "" ;
	    String coPhone = "" ;
	    try {
	    	coName = URLEncoder.encode( userInfo.getName() + "" ,"utf-8");
	    	coPhone = URLEncoder.encode( userInfo.getPhone() + "" ,"utf-8");
	    	logger.debug("Save cookie info , coName =" + coName + " , coPhone =" + coPhone );
	    } catch (UnsupportedEncodingException e) {
	    	logger.warn(" encod error =" + e , e );
	    }
	    Cookie cookieName = new Cookie("coName",coName );
	    Cookie cookiePhone = new Cookie("coPhone", coPhone);
	    Cookie userNumber = new Cookie("userNumber", coPhone);
	  	
	    cookieName.setMaxAge( maxAge );
	    cookiePhone.setMaxAge( maxAge );
	    userNumber.setMaxAge( maxAge );
	    cookieName.setPath("/");
	    cookiePhone.setPath("/");
	    userNumber.setPath("/");
	    super.getResponse().addCookie( cookieName );
	    super.getResponse().addCookie( cookiePhone );
	    super.getResponse().addCookie( userNumber );
	    
	    this.logger.debug("Session , Cookie 设置成功 ! ");
	    
	    return SUCCESS;
	  }
	  
	  /**
	   * 检查数据中是否包含当前要注册的电话号码
	   */
	  public void phoneCheck()
	  {
		  this.logger.debug(" phoneCheck => phone : " + this.phone);
		  boolean result = this.userInfoService.selectByPhone(this.phone);
		  if (result) {
			  try {
				  super.getResponse().getWriter().print("当前手机号已经被注册,请重新输入!");
			  } catch (IOException e) {
				  this.logger.warn(" error = " + e, e);
			  }
		  }
	  }

	  public void nameCheck()
	  {
	    this.logger.debug(" nameCheck => name : " + this.name);
	    boolean result = this.userInfoService.selectByName(this.name);
	    if (result)
	      try {
	        super.getResponse().getWriter().print("当前姓名已经被注册,请重新输入!");
	      } catch (IOException e) {
	        this.logger.warn(" error = " + e, e);
	      }
	  }
  
	  public String getName() {
	    return this.name;
	  }
	
	  public void setName(String name) {
	    this.name = name;
	  }


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public String getHospital() {
		return hospital;
	}


	public void setHospital(String hospital) {
		this.hospital = hospital;
	}


	public String getSection() {
		return section;
	}


	public void setSection(String section) {
		this.section = section;
	}
	
	  
}