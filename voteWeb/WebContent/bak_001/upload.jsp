<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="keywords" content="">
		<meta name="description" content="">
		<meta name="viewport"
			content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<title>微信在线投票系统后台</title>
		 <link href="images/phone/css/tp.css" rel="stylesheet" type="text/css">
		<script src="js/jquery-1.10.1.min.js"></script>
		<script type="text/javascript">
	$().ready(function() {
		$('#submit_button').click(function() {
			$("#upload").submit();
		});
	});
</script>

	</head>
	<body>
	  <div class="div_box">
        <div><h1>微信在线投票系统后台</h1></div>
            <form method="post" action="upload" id="upload" enctype="multipart/form-data">
                <div class="div1">
                    <span class="filePicker">图片类型</span><select name="type">
                        <option value="1">
                            手机端图片
                        </option>
                        <option value="2">
                            电脑端图片
                        </option>
                    </select>
                </div>
                <div class="div2">
                    <span class="filePicker">图片名称</span><input class="filename" type="text" name="imgname">
                </div>
                <div class="div3">
                    <div class="filePicker xz">点击选择文件</div>
                    <div class="tijiao"><input type="file" name="file"></div>
                    <input class="filePicker tj" type="button" value="提交" id="submit_button">
                </div>
                <div class="div4">${msg}</div>
            </form>
    </div>
	</body>
</html>
