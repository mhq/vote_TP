$(function() {
	// 全局变量
	obj = {
		search : function() {
			alert('111')

		},

	}

	// datagrid
	$('#dg').datagrid({
		url : 'js/test.json',
		// url:"verifyServlet",
		striped : true,
		rownumbers : true,
		title : '客户信息',
		width : 1800,
		height : 500,
		inconCls : 'icon-search',
		pagination : true,
		pageSize : 10,
		pageList : [ 10, 20, 30 ],
		// fit:true,
		columns : [ [ {
			field : 'fromUserName',
			title : '发送者',
			width : 100,
		},

		{
			field : 'createTime',
			title : '提交时间',
			width : 100,
		}, {
			field : 'msgType',
			title : '类型',
			width : 100,
		}, {
			field : 'content',
			title : '文本内容',
			width : 100,
		}, {
			field : 'msgId',
			title : 'msgId',
			width : 100,
		}, {
			field : 'mediaId',
			title : '多媒体ID',
			width : 100,
		}, {
			field : 'picUrl',
			title : '图片地址',
			width : 100,
		}, {
			field : 'url',
			title : '路径',
			width : 100,
		},

		] ],
	});
	$('#d').timespinner({
		value : '00:00:00',
		min : '00:00',
		max : '24:00',
	});

	$('#box').timespinner('setValue', '12:00:00');

});

$(function() {

	$('#datetimebox1').datetimebox({
		value : '1/1/2015 11:11:11',

	})

});