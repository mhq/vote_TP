/**
 * Created by Ajax on 2016/2/19.
 */
(function(d, e) {
    var f = {
        supportsFullScreen:false,
        isFullScreen:function() {
            return false;
        },
        requestFullScreen:function() {},
        cancelFullScreen:function() {},
        fullScreenEventName:"",
        prefix:""
    }, c = "webkit moz o ms khtml".split(" ");
    if (typeof document.cancelFullScreen != "undefined") {
        f.supportsFullScreen = true;
    } else {
        for (var b = 0, a = c.length; b < a; b++) {
            f.prefix = c[b];
            if (typeof document[f.prefix + "CancelFullScreen"] != "undefined") {
                f.supportsFullScreen = true;
                break;
            }
        }
    }
    if (f.supportsFullScreen) {
        f.fullScreenEventName = f.prefix + "fullscreenchange";
        f.isFullScreen = function() {
            switch (this.prefix) {
                case "":
                    return document.fullScreen;

                case "webkit":
                    return document.webkitIsFullScreen;

                default:
                    return document[this.prefix + "FullScreen"];
            }
        };
        f.requestFullScreen = function(g) {
            return this.prefix === "" ? g.requestFullScreen() :g[this.prefix + "RequestFullScreen"]();
        };
        f.cancelFullScreen = function(g) {
            return this.prefix === "" ? document.cancelFullScreen() :document[this.prefix + "CancelFullScreen"]();
        };
    }
    if (typeof jQuery != "undefined") {
        jQuery.fn.requestFullScreen = function() {
            return this.each(function() {
                var g = jQuery(this);
                if (f.supportsFullScreen) {
                    f.requestFullScreen(g);
                }
            });
        };
    }
    d.fullScreenApi = f;
    e.toggleFullScreen = function() {
        if (f.isFullScreen()) {
            f.cancelFullScreen(document.documentElement);
        } else {
            f.requestFullScreen(document.documentElement);
        }
    };
})(window, jQuery);
