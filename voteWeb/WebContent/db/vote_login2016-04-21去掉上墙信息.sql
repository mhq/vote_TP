/*
Navicat MySQL Data Transfer

Source Server         : db
Source Server Version : 50547
Source Host           : localhost:3306
Source Database       : vote_login

Target Server Type    : MYSQL
Target Server Version : 50547
File Encoding         : 65001

Date: 2016-04-21 20:36:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for yh_answer
-- ----------------------------
DROP TABLE IF EXISTS `yh_answer`;
CREATE TABLE `yh_answer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `answer_value` varchar(255) DEFAULT NULL,
  `questionid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yh_answer
-- ----------------------------
INSERT INTO `yh_answer` VALUES ('1', '1', '1');
INSERT INTO `yh_answer` VALUES ('2', '2', '1');
INSERT INTO `yh_answer` VALUES ('3', '3', '1');
INSERT INTO `yh_answer` VALUES ('4', '1', '2');
INSERT INTO `yh_answer` VALUES ('5', '2', '2');
INSERT INTO `yh_answer` VALUES ('6', '3', '2');
INSERT INTO `yh_answer` VALUES ('7', '1', '3');
INSERT INTO `yh_answer` VALUES ('8', '2', '3');
INSERT INTO `yh_answer` VALUES ('9', '3', '3');
INSERT INTO `yh_answer` VALUES ('10', '1', '4');
INSERT INTO `yh_answer` VALUES ('11', '2', '4');
INSERT INTO `yh_answer` VALUES ('12', '3', '4');
INSERT INTO `yh_answer` VALUES ('13', '1', '5');
INSERT INTO `yh_answer` VALUES ('14', '2', '5');
INSERT INTO `yh_answer` VALUES ('15', '3', '5');
INSERT INTO `yh_answer` VALUES ('16', '4', '5');
INSERT INTO `yh_answer` VALUES ('17', '5', '5');
INSERT INTO `yh_answer` VALUES ('18', '6', '5');
INSERT INTO `yh_answer` VALUES ('19', '7', '5');

-- ----------------------------
-- Table structure for yh_current_quesiont_num
-- ----------------------------
DROP TABLE IF EXISTS `yh_current_quesiont_num`;
CREATE TABLE `yh_current_quesiont_num` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `questionid` bigint(20) DEFAULT NULL,
  `start_time` bigint(20) DEFAULT NULL,
  `start_vote` bit(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yh_current_quesiont_num
-- ----------------------------
INSERT INTO `yh_current_quesiont_num` VALUES ('1', '1', null, '\0');

-- ----------------------------
-- Table structure for yh_question
-- ----------------------------
DROP TABLE IF EXISTS `yh_question`;
CREATE TABLE `yh_question` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `choose_count` int(11) NOT NULL,
  `question_name` varchar(255) DEFAULT NULL,
  `question_number` varchar(255) DEFAULT NULL,
  `right_answers` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yh_question
-- ----------------------------
INSERT INTO `yh_question` VALUES ('1', '1', '第一题', '1', '4');
INSERT INTO `yh_question` VALUES ('2', '1', '第二题', '2', '1');
INSERT INTO `yh_question` VALUES ('3', '1', '第三题', '3', '1');
INSERT INTO `yh_question` VALUES ('4', '4', '第四题', '4', '1');
INSERT INTO `yh_question` VALUES ('5', '1', '第五题目', '5', '1');

-- ----------------------------
-- Table structure for yh_user_info
-- ----------------------------
DROP TABLE IF EXISTS `yh_user_info`;
CREATE TABLE `yh_user_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `section` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yh_user_info
-- ----------------------------
INSERT INTO `yh_user_info` VALUES ('1', 'test', '18600341337');
INSERT INTO `yh_user_info` VALUES ('2', 'test', '18888888801');
INSERT INTO `yh_user_info` VALUES ('3', 'test', '18888888802');
INSERT INTO `yh_user_info` VALUES ('4', 'test', '18888888803');
INSERT INTO `yh_user_info` VALUES ('5', 'test', '18888888804');
INSERT INTO `yh_user_info` VALUES ('6', 'test', '18888888805');
INSERT INTO `yh_user_info` VALUES ('7', 'test', '18888888806');
INSERT INTO `yh_user_info` VALUES ('8', 'test', '18888888807');
INSERT INTO `yh_user_info` VALUES ('9', 'test', '18888888808');
INSERT INTO `yh_user_info` VALUES ('10', 'test', '18888888809');
INSERT INTO `yh_user_info` VALUES ('11', 'test', '18888888810');
INSERT INTO `yh_user_info` VALUES ('12', 'test', '18888888811');
INSERT INTO `yh_user_info` VALUES ('13', 'test', '18888888812');
INSERT INTO `yh_user_info` VALUES ('14', 'test', '18888888813');
INSERT INTO `yh_user_info` VALUES ('15', 'test', '18888888814');
INSERT INTO `yh_user_info` VALUES ('16', 'test', '18888888815');
INSERT INTO `yh_user_info` VALUES ('17', 'test', '18888888816');
INSERT INTO `yh_user_info` VALUES ('18', 'test', '18888888817');
INSERT INTO `yh_user_info` VALUES ('19', 'test', '18888888818');
INSERT INTO `yh_user_info` VALUES ('20', 'test', '18888888819');
INSERT INTO `yh_user_info` VALUES ('21', 'test', '18888888820');

-- ----------------------------
-- Table structure for yh_vote
-- ----------------------------
DROP TABLE IF EXISTS `yh_vote`;
CREATE TABLE `yh_vote` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `answer` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `questionid` bigint(20) DEFAULT NULL,
  `use_time` bigint(20) DEFAULT NULL,
  `userid` bigint(20) DEFAULT NULL,
  `user_number` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yh_vote
-- ----------------------------
INSERT INTO `yh_vote` VALUES ('10', '2', '2016-04-07 11:35:52', '1', null, '10001', '0');
INSERT INTO `yh_vote` VALUES ('11', '3', '2016-04-07 11:36:21', '1', null, '10002', '0');
