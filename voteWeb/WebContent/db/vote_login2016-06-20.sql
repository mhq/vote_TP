/*
Navicat MySQL Data Transfer

Source Server         : db
Source Server Version : 50547
Source Host           : localhost:3306
Source Database       : vote_login

Target Server Type    : MYSQL
Target Server Version : 50547
File Encoding         : 65001

Date: 2016-06-20 19:27:00
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `yh_answer`
-- ----------------------------
DROP TABLE IF EXISTS `yh_answer`;
CREATE TABLE `yh_answer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `answer_value` varchar(255) DEFAULT NULL,
  `questionid` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yh_answer
-- ----------------------------
INSERT INTO yh_answer VALUES ('1', '1', '1');
INSERT INTO yh_answer VALUES ('2', '2', '1');
INSERT INTO yh_answer VALUES ('3', '3', '1');
INSERT INTO yh_answer VALUES ('4', '4', '1');
INSERT INTO yh_answer VALUES ('5', '5', '1');
INSERT INTO yh_answer VALUES ('6', '6', '1');
INSERT INTO yh_answer VALUES ('7', '1', '2');
INSERT INTO yh_answer VALUES ('8', '2', '2');
INSERT INTO yh_answer VALUES ('9', '3', '2');
INSERT INTO yh_answer VALUES ('10', '4', '2');
INSERT INTO yh_answer VALUES ('11', '5', '2');
INSERT INTO yh_answer VALUES ('12', '6', '2');
INSERT INTO yh_answer VALUES ('13', '7', '2');
INSERT INTO yh_answer VALUES ('14', '1', '3');
INSERT INTO yh_answer VALUES ('15', '2', '3');
INSERT INTO yh_answer VALUES ('16', '3', '3');
INSERT INTO yh_answer VALUES ('17', '4', '3');
INSERT INTO yh_answer VALUES ('18', '5', '3');
INSERT INTO yh_answer VALUES ('19', '1', '4');
INSERT INTO yh_answer VALUES ('20', '2', '4');
INSERT INTO yh_answer VALUES ('21', '3', '4');
INSERT INTO yh_answer VALUES ('22', '4', '4');
INSERT INTO yh_answer VALUES ('23', '1', '5');
INSERT INTO yh_answer VALUES ('24', '2', '5');
INSERT INTO yh_answer VALUES ('25', '3', '5');
INSERT INTO yh_answer VALUES ('26', '1', '6');
INSERT INTO yh_answer VALUES ('27', '2', '6');
INSERT INTO yh_answer VALUES ('28', '3', '6');
INSERT INTO yh_answer VALUES ('29', '4', '6');
INSERT INTO yh_answer VALUES ('30', '1', '7');
INSERT INTO yh_answer VALUES ('31', '2', '7');
INSERT INTO yh_answer VALUES ('32', '3', '7');
INSERT INTO yh_answer VALUES ('33', '4', '7');
INSERT INTO yh_answer VALUES ('34', '1', '8');
INSERT INTO yh_answer VALUES ('35', '2', '8');
INSERT INTO yh_answer VALUES ('36', '3', '8');
INSERT INTO yh_answer VALUES ('37', '4', '8');
INSERT INTO yh_answer VALUES ('38', '1', '9');
INSERT INTO yh_answer VALUES ('39', '2', '9');
INSERT INTO yh_answer VALUES ('40', '3', '9');

-- ----------------------------
-- Table structure for `yh_current_quesiont_num`
-- ----------------------------
DROP TABLE IF EXISTS `yh_current_quesiont_num`;
CREATE TABLE `yh_current_quesiont_num` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `questionid` bigint(20) DEFAULT NULL,
  `start_time` bigint(20) DEFAULT NULL,
  `start_vote` bit(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yh_current_quesiont_num
-- ----------------------------
INSERT INTO yh_current_quesiont_num VALUES ('1', '1', null, '');

-- ----------------------------
-- Table structure for `yh_question`
-- ----------------------------
DROP TABLE IF EXISTS `yh_question`;
CREATE TABLE `yh_question` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `choose_count` int(11) NOT NULL,
  `question_name` varchar(255) DEFAULT NULL,
  `question_number` varchar(255) DEFAULT NULL,
  `right_answers` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yh_question
-- ----------------------------
INSERT INTO yh_question VALUES ('1', '6', '第一题', '1', '1');
INSERT INTO yh_question VALUES ('2', '7', '第二题', '2', '1');
INSERT INTO yh_question VALUES ('3', '5', '第三题', '3', '1');
INSERT INTO yh_question VALUES ('4', '4', '第四题', '4', '1');
INSERT INTO yh_question VALUES ('5', '3', '第五题', '5', '1');
INSERT INTO yh_question VALUES ('6', '4', '第六题', '6', '1');
INSERT INTO yh_question VALUES ('7', '4', '第七题', '7', '1');
INSERT INTO yh_question VALUES ('8', '4', '第八题', '8', '1');
INSERT INTO yh_question VALUES ('9', '3', '第九题', '9', '1');

-- ----------------------------
-- Table structure for `yh_user_info`
-- ----------------------------
DROP TABLE IF EXISTS `yh_user_info`;
CREATE TABLE `yh_user_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `section` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=369 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yh_user_info
-- ----------------------------
INSERT INTO yh_user_info VALUES ('1', 'test', '18600341337');
INSERT INTO yh_user_info VALUES ('2', 'test', '18888888801');
INSERT INTO yh_user_info VALUES ('170', 'test', '18888888802');
INSERT INTO yh_user_info VALUES ('171', 'test', '18888888803');
INSERT INTO yh_user_info VALUES ('172', 'test', '18888888804');
INSERT INTO yh_user_info VALUES ('173', 'test', '18888888805');
INSERT INTO yh_user_info VALUES ('174', 'test', '18888888806');
INSERT INTO yh_user_info VALUES ('175', 'test', '18888888807');
INSERT INTO yh_user_info VALUES ('176', 'test', '18888888808');
INSERT INTO yh_user_info VALUES ('177', 'test', '18888888809');
INSERT INTO yh_user_info VALUES ('178', 'test', '18888888810');
INSERT INTO yh_user_info VALUES ('179', 'test', '18888888811');
INSERT INTO yh_user_info VALUES ('180', 'test', '18888888812');
INSERT INTO yh_user_info VALUES ('181', 'test', '18888888813');
INSERT INTO yh_user_info VALUES ('182', 'test', '18888888814');
INSERT INTO yh_user_info VALUES ('183', 'test', '18888888815');
INSERT INTO yh_user_info VALUES ('184', 'test', '18888888816');
INSERT INTO yh_user_info VALUES ('185', 'test', '18888888817');
INSERT INTO yh_user_info VALUES ('186', 'test', '18888888818');
INSERT INTO yh_user_info VALUES ('187', 'test', '18888888819');
INSERT INTO yh_user_info VALUES ('188', 'test', '18888888820');
INSERT INTO yh_user_info VALUES ('189', 'test', '18888888821');
INSERT INTO yh_user_info VALUES ('190', 'test', '18888888822');
INSERT INTO yh_user_info VALUES ('191', 'test', '18888888823');
INSERT INTO yh_user_info VALUES ('192', 'test', '18888888824');
INSERT INTO yh_user_info VALUES ('193', 'test', '18888888825');
INSERT INTO yh_user_info VALUES ('194', 'test', '18888888826');
INSERT INTO yh_user_info VALUES ('195', 'test', '18888888827');
INSERT INTO yh_user_info VALUES ('196', 'test', '18888888828');
INSERT INTO yh_user_info VALUES ('197', 'test', '18888888829');
INSERT INTO yh_user_info VALUES ('198', 'test', '18888888830');
INSERT INTO yh_user_info VALUES ('199', 'test', '18888888831');
INSERT INTO yh_user_info VALUES ('200', 'test', '18888888832');
INSERT INTO yh_user_info VALUES ('201', 'test', '13021733383');
INSERT INTO yh_user_info VALUES ('202', 'test', '13311581088');
INSERT INTO yh_user_info VALUES ('203', 'test', '13347198383');
INSERT INTO yh_user_info VALUES ('204', 'test', '13501050651');
INSERT INTO yh_user_info VALUES ('205', 'test', '13509152099');
INSERT INTO yh_user_info VALUES ('206', 'test', '13584097091');
INSERT INTO yh_user_info VALUES ('207', 'test', '13584885379');
INSERT INTO yh_user_info VALUES ('208', 'test', '13602127632');
INSERT INTO yh_user_info VALUES ('209', 'test', '13605142342');
INSERT INTO yh_user_info VALUES ('210', 'test', '13605310718');
INSERT INTO yh_user_info VALUES ('211', 'test', '13608357776');
INSERT INTO yh_user_info VALUES ('212', 'test', '13621262462');
INSERT INTO yh_user_info VALUES ('213', 'test', '13638332366');
INSERT INTO yh_user_info VALUES ('214', 'test', '13651198922');
INSERT INTO yh_user_info VALUES ('215', 'test', '13652183545');
INSERT INTO yh_user_info VALUES ('216', 'test', '13667686098');
INSERT INTO yh_user_info VALUES ('217', 'test', '13682238296');
INSERT INTO yh_user_info VALUES ('218', 'test', '13701156489');
INSERT INTO yh_user_info VALUES ('219', 'test', '13701389625');
INSERT INTO yh_user_info VALUES ('220', 'test', '13701787996');
INSERT INTO yh_user_info VALUES ('221', 'test', '13755197569');
INSERT INTO yh_user_info VALUES ('222', 'test', '13786107372');
INSERT INTO yh_user_info VALUES ('223', 'test', '13791122627');
INSERT INTO yh_user_info VALUES ('224', 'test', '13802989985');
INSERT INTO yh_user_info VALUES ('225', 'test', '13810948338');
INSERT INTO yh_user_info VALUES ('226', 'test', '13811794835');
INSERT INTO yh_user_info VALUES ('227', 'test', '13819734866');
INSERT INTO yh_user_info VALUES ('228', 'test', '13820254253');
INSERT INTO yh_user_info VALUES ('230', 'test', '13821452508');
INSERT INTO yh_user_info VALUES ('231', 'test', '13873150736');
INSERT INTO yh_user_info VALUES ('232', 'test', '13883580112');
INSERT INTO yh_user_info VALUES ('233', 'test', '13892888189');
INSERT INTO yh_user_info VALUES ('234', 'test', '13896000239');
INSERT INTO yh_user_info VALUES ('235', 'test', '13901113623');
INSERT INTO yh_user_info VALUES ('236', 'test', '13909910116');
INSERT INTO yh_user_info VALUES ('237', 'test', '13913965795');
INSERT INTO yh_user_info VALUES ('238', 'test', '13951693856');
INSERT INTO yh_user_info VALUES ('239', 'test', '13974569622');
INSERT INTO yh_user_info VALUES ('240', 'test', '13974898880');
INSERT INTO yh_user_info VALUES ('241', 'test', '13983668837');
INSERT INTO yh_user_info VALUES ('242', 'test', '13996176065');
INSERT INTO yh_user_info VALUES ('243', 'test', '15041232606');
INSERT INTO yh_user_info VALUES ('244', 'test', '15076632902');
INSERT INTO yh_user_info VALUES ('245', 'test', '15102265596');
INSERT INTO yh_user_info VALUES ('246', 'test', '15163451152');
INSERT INTO yh_user_info VALUES ('247', 'test', '15200801223');
INSERT INTO yh_user_info VALUES ('248', 'test', '15215048257');
INSERT INTO yh_user_info VALUES ('249', 'test', '15232445796');
INSERT INTO yh_user_info VALUES ('250', 'test', '15244120026');
INSERT INTO yh_user_info VALUES ('251', 'test', '15274387667');
INSERT INTO yh_user_info VALUES ('252', 'test', '15330070363');
INSERT INTO yh_user_info VALUES ('253', 'test', '15353699551');
INSERT INTO yh_user_info VALUES ('254', 'test', '15388021652');
INSERT INTO yh_user_info VALUES ('255', 'test', '15506155768');
INSERT INTO yh_user_info VALUES ('256', 'test', '15619185093');
INSERT INTO yh_user_info VALUES ('257', 'test', '15665860219');
INSERT INTO yh_user_info VALUES ('258', 'test', '15673388660');
INSERT INTO yh_user_info VALUES ('259', 'test', '15675886807');
INSERT INTO yh_user_info VALUES ('260', 'test', '15840415835');
INSERT INTO yh_user_info VALUES ('261', 'test', '15848810716');
INSERT INTO yh_user_info VALUES ('262', 'test', '15900225626');
INSERT INTO yh_user_info VALUES ('263', 'test', '17709870963');
INSERT INTO yh_user_info VALUES ('264', 'test', '18052098224');
INSERT INTO yh_user_info VALUES ('265', 'test', '18073213223');
INSERT INTO yh_user_info VALUES ('266', 'test', '18109176958');
INSERT INTO yh_user_info VALUES ('267', 'test', '18136656837');
INSERT INTO yh_user_info VALUES ('268', 'test', '18163695067');
INSERT INTO yh_user_info VALUES ('269', 'test', '18202644210');
INSERT INTO yh_user_info VALUES ('270', 'test', '18366110853');
INSERT INTO yh_user_info VALUES ('271', 'test', '18392630971');
INSERT INTO yh_user_info VALUES ('272', 'test', '18505386878');
INSERT INTO yh_user_info VALUES ('273', 'test', '18520776067');
INSERT INTO yh_user_info VALUES ('274', 'test', '18560086993');
INSERT INTO yh_user_info VALUES ('275', 'test', '18560087021');
INSERT INTO yh_user_info VALUES ('276', 'test', '18569459957');
INSERT INTO yh_user_info VALUES ('277', 'test', '18570653560');
INSERT INTO yh_user_info VALUES ('278', 'test', '18584605860');
INSERT INTO yh_user_info VALUES ('279', 'test', '18586053024');
INSERT INTO yh_user_info VALUES ('280', 'test', '18600341337');
INSERT INTO yh_user_info VALUES ('281', 'test', '18610050095');
INSERT INTO yh_user_info VALUES ('282', 'test', '18623015252');
INSERT INTO yh_user_info VALUES ('283', 'test', '18653139856');
INSERT INTO yh_user_info VALUES ('284', 'test', '18678756540');
INSERT INTO yh_user_info VALUES ('285', 'test', '18680770423');
INSERT INTO yh_user_info VALUES ('286', 'test', '18691869100');
INSERT INTO yh_user_info VALUES ('287', 'test', '18696562026');
INSERT INTO yh_user_info VALUES ('289', 'test', '18698722584');
INSERT INTO yh_user_info VALUES ('290', 'test', '18761882990');
INSERT INTO yh_user_info VALUES ('291', 'test', '18874776751');
INSERT INTO yh_user_info VALUES ('292', 'test', '18911255603');
INSERT INTO yh_user_info VALUES ('293', 'test', '18949836573');
INSERT INTO yh_user_info VALUES ('294', 'test', '18888888833');
INSERT INTO yh_user_info VALUES ('295', 'test', '18888888834');
INSERT INTO yh_user_info VALUES ('296', 'test', '18888888835');
INSERT INTO yh_user_info VALUES ('297', 'test', '18888888836');
INSERT INTO yh_user_info VALUES ('298', 'test', '18888888837');
INSERT INTO yh_user_info VALUES ('299', 'test', '18888888838');
INSERT INTO yh_user_info VALUES ('300', 'test', '18888888839');
INSERT INTO yh_user_info VALUES ('301', 'test', '18888888840');
INSERT INTO yh_user_info VALUES ('302', 'test', '18888888841');
INSERT INTO yh_user_info VALUES ('303', 'test', '18888888842');
INSERT INTO yh_user_info VALUES ('304', 'test', '18888888843');
INSERT INTO yh_user_info VALUES ('305', 'test', '18888888844');
INSERT INTO yh_user_info VALUES ('306', 'test', '18888888845');
INSERT INTO yh_user_info VALUES ('307', 'test', '18888888846');
INSERT INTO yh_user_info VALUES ('308', 'test', '18888888847');
INSERT INTO yh_user_info VALUES ('309', 'test', '18888888848');
INSERT INTO yh_user_info VALUES ('310', 'test', '18888888849');
INSERT INTO yh_user_info VALUES ('311', 'test', '18888888850');
INSERT INTO yh_user_info VALUES ('312', 'test', '18888888851');
INSERT INTO yh_user_info VALUES ('313', 'test', '18888888852');
INSERT INTO yh_user_info VALUES ('314', 'test', '18888888853');
INSERT INTO yh_user_info VALUES ('315', 'test', '18888888854');
INSERT INTO yh_user_info VALUES ('316', 'test', '18888888855');
INSERT INTO yh_user_info VALUES ('317', 'test', '18888888856');
INSERT INTO yh_user_info VALUES ('318', 'test', '18888888857');
INSERT INTO yh_user_info VALUES ('319', 'test', '18888888858');
INSERT INTO yh_user_info VALUES ('320', 'test', '18888888859');
INSERT INTO yh_user_info VALUES ('321', 'test', '18888888860');
INSERT INTO yh_user_info VALUES ('322', 'test', '18888888861');
INSERT INTO yh_user_info VALUES ('323', 'test', '18888888862');
INSERT INTO yh_user_info VALUES ('324', 'test', '18888888863');
INSERT INTO yh_user_info VALUES ('325', 'test', '18888888864');
INSERT INTO yh_user_info VALUES ('326', 'test', '18888888865');
INSERT INTO yh_user_info VALUES ('327', 'test', '18888888866');
INSERT INTO yh_user_info VALUES ('328', 'test', '18888888867');
INSERT INTO yh_user_info VALUES ('329', 'test', '18888888868');
INSERT INTO yh_user_info VALUES ('330', 'test', '18888888869');
INSERT INTO yh_user_info VALUES ('331', 'test', '18888888870');
INSERT INTO yh_user_info VALUES ('332', 'test', '18888888871');
INSERT INTO yh_user_info VALUES ('333', 'test', '18888888872');
INSERT INTO yh_user_info VALUES ('334', 'test', '18888888873');
INSERT INTO yh_user_info VALUES ('335', 'test', '18888888874');
INSERT INTO yh_user_info VALUES ('336', 'test', '18888888875');
INSERT INTO yh_user_info VALUES ('337', 'test', '18888888876');
INSERT INTO yh_user_info VALUES ('338', 'test', '18888888877');
INSERT INTO yh_user_info VALUES ('339', 'test', '18888888878');
INSERT INTO yh_user_info VALUES ('340', 'test', '18888888879');
INSERT INTO yh_user_info VALUES ('341', 'test', '18888888880');
INSERT INTO yh_user_info VALUES ('342', 'test', '18888888881');
INSERT INTO yh_user_info VALUES ('343', 'test', '18888888882');
INSERT INTO yh_user_info VALUES ('344', 'test', '18888888883');
INSERT INTO yh_user_info VALUES ('345', 'test', '18888888884');
INSERT INTO yh_user_info VALUES ('346', 'test', '18888888885');
INSERT INTO yh_user_info VALUES ('347', 'test', '18888888886');
INSERT INTO yh_user_info VALUES ('348', 'test', '18888888887');
INSERT INTO yh_user_info VALUES ('349', 'test', '18888888888');
INSERT INTO yh_user_info VALUES ('350', 'test', '18888888889');
INSERT INTO yh_user_info VALUES ('351', 'test', '18888888890');
INSERT INTO yh_user_info VALUES ('352', 'test', '18888888891');
INSERT INTO yh_user_info VALUES ('353', 'test', '18888888892');
INSERT INTO yh_user_info VALUES ('354', 'test', '18888888893');
INSERT INTO yh_user_info VALUES ('355', 'test', '18888888894');
INSERT INTO yh_user_info VALUES ('356', 'test', '18888888895');
INSERT INTO yh_user_info VALUES ('357', 'test', '18888888896');
INSERT INTO yh_user_info VALUES ('358', 'test', '18888888897');
INSERT INTO yh_user_info VALUES ('359', 'test', '18888888898');
INSERT INTO yh_user_info VALUES ('360', 'test', '18888888899');
INSERT INTO yh_user_info VALUES ('361', 'test', '18888888100');
INSERT INTO yh_user_info VALUES ('362', 'test', '18769395805');
INSERT INTO yh_user_info VALUES ('363', 'test', '13920220069');
INSERT INTO yh_user_info VALUES ('364', 'test', '13563335205');
INSERT INTO yh_user_info VALUES ('365', 'test', '15951268038');
INSERT INTO yh_user_info VALUES ('366', 'test', '18795815214');
INSERT INTO yh_user_info VALUES ('367', 'test', '13602094177');
INSERT INTO yh_user_info VALUES ('368', 'test', '13961903136');

-- ----------------------------
-- Table structure for `yh_vote`
-- ----------------------------
DROP TABLE IF EXISTS `yh_vote`;
CREATE TABLE `yh_vote` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `answer` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `questionid` bigint(20) DEFAULT NULL,
  `use_time` bigint(20) DEFAULT NULL,
  `userid` bigint(20) DEFAULT NULL,
  `user_number` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1260 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yh_vote
-- ----------------------------
INSERT INTO yh_vote VALUES ('898', '3', '2016-06-18 08:30:28', '1', null, '10008', '0');
INSERT INTO yh_vote VALUES ('899', '3', '2016-06-18 08:30:31', '1', null, '10009', '0');
INSERT INTO yh_vote VALUES ('900', '3', '2016-06-18 08:30:31', '1', null, '10010', '0');
INSERT INTO yh_vote VALUES ('901', '3', '2016-06-18 08:30:32', '1', null, '10011', '0');
INSERT INTO yh_vote VALUES ('902', '2', '2016-06-18 08:30:32', '1', null, '10012', '0');
INSERT INTO yh_vote VALUES ('903', '3', '2016-06-18 08:30:32', '1', null, '10013', '0');
INSERT INTO yh_vote VALUES ('904', '1', '2016-06-18 08:30:34', '1', null, '10014', '0');
INSERT INTO yh_vote VALUES ('905', '3', '2016-06-18 08:30:34', '1', null, '10015', '0');
INSERT INTO yh_vote VALUES ('906', '3', '2016-06-18 08:30:35', '1', null, '10016', '0');
INSERT INTO yh_vote VALUES ('907', '2', '2016-06-18 08:30:36', '1', null, '10017', '0');
INSERT INTO yh_vote VALUES ('908', '3', '2016-06-18 08:30:37', '1', null, '10018', '0');
INSERT INTO yh_vote VALUES ('909', '3', '2016-06-18 08:30:37', '1', null, '10019', '0');
INSERT INTO yh_vote VALUES ('910', '3', '2016-06-18 08:30:38', '1', null, '10020', '0');
INSERT INTO yh_vote VALUES ('911', '3', '2016-06-18 08:30:38', '1', null, '10021', '0');
INSERT INTO yh_vote VALUES ('912', '2', '2016-06-18 08:30:38', '1', null, '10022', '0');
INSERT INTO yh_vote VALUES ('913', '2', '2016-06-18 08:30:39', '1', null, '10023', '0');
INSERT INTO yh_vote VALUES ('914', '2', '2016-06-18 08:30:41', '1', null, '10024', '0');
INSERT INTO yh_vote VALUES ('915', '2', '2016-06-18 08:30:43', '1', null, '10025', '0');
INSERT INTO yh_vote VALUES ('916', '2', '2016-06-18 08:30:43', '1', null, '10026', '0');
INSERT INTO yh_vote VALUES ('917', '3', '2016-06-18 08:30:44', '1', null, '10027', '0');
INSERT INTO yh_vote VALUES ('918', '3', '2016-06-18 08:30:45', '1', null, '10028', '0');
INSERT INTO yh_vote VALUES ('919', '3', '2016-06-18 08:30:45', '1', null, '10029', '0');
INSERT INTO yh_vote VALUES ('920', '1', '2016-06-18 08:30:45', '1', null, '10030', '0');
INSERT INTO yh_vote VALUES ('921', '2', '2016-06-18 08:30:46', '1', null, '10031', '0');
INSERT INTO yh_vote VALUES ('922', '3', '2016-06-18 08:30:46', '1', null, '10032', '0');
INSERT INTO yh_vote VALUES ('923', '3', '2016-06-18 08:30:46', '1', null, '10033', '0');
INSERT INTO yh_vote VALUES ('924', '3', '2016-06-18 08:30:47', '1', null, '10034', '0');
INSERT INTO yh_vote VALUES ('925', '3', '2016-06-18 08:30:47', '1', null, '10035', '0');
INSERT INTO yh_vote VALUES ('926', '3', '2016-06-18 08:30:47', '1', null, '10036', '0');
INSERT INTO yh_vote VALUES ('927', '2', '2016-06-18 08:30:47', '1', null, '10037', '0');
INSERT INTO yh_vote VALUES ('928', '2', '2016-06-18 08:30:47', '1', null, '10038', '0');
INSERT INTO yh_vote VALUES ('929', '2', '2016-06-18 08:30:49', '1', null, '10007', '0');
INSERT INTO yh_vote VALUES ('930', '3', '2016-06-18 08:30:51', '1', null, '10039', '0');
INSERT INTO yh_vote VALUES ('931', '2', '2016-06-18 08:30:51', '1', null, '10040', '0');
INSERT INTO yh_vote VALUES ('932', '3', '2016-06-18 08:30:52', '1', null, '10042', '0');
INSERT INTO yh_vote VALUES ('933', '2', '2016-06-18 08:30:53', '1', null, '10043', '0');
INSERT INTO yh_vote VALUES ('934', '2', '2016-06-18 08:30:58', '1', null, '10044', '0');
INSERT INTO yh_vote VALUES ('935', '2', '2016-06-18 08:30:58', '1', null, '10045', '0');
INSERT INTO yh_vote VALUES ('936', '3', '2016-06-18 08:30:58', '1', null, '10046', '0');
INSERT INTO yh_vote VALUES ('937', '2', '2016-06-18 08:30:58', '1', null, '10047', '0');
INSERT INTO yh_vote VALUES ('938', '3', '2016-06-18 08:30:59', '1', null, '10048', '0');
INSERT INTO yh_vote VALUES ('939', '3', '2016-06-18 08:30:59', '1', null, '10049', '0');
INSERT INTO yh_vote VALUES ('940', '3', '2016-06-18 08:30:59', '1', null, '10005', '0');
INSERT INTO yh_vote VALUES ('941', '3', '2016-06-18 08:31:03', '1', null, '10050', '0');
INSERT INTO yh_vote VALUES ('942', '3', '2016-06-18 08:31:04', '1', null, '10051', '0');
INSERT INTO yh_vote VALUES ('943', '2', '2016-06-18 08:31:05', '1', null, '10052', '0');
INSERT INTO yh_vote VALUES ('944', '2', '2016-06-18 08:31:06', '1', null, '10053', '0');
INSERT INTO yh_vote VALUES ('945', '3', '2016-06-18 08:31:06', '1', null, '10054', '0');
INSERT INTO yh_vote VALUES ('946', '2', '2016-06-18 08:31:07', '1', null, '10055', '0');
INSERT INTO yh_vote VALUES ('947', '3', '2016-06-18 08:31:07', '1', null, '10056', '0');
INSERT INTO yh_vote VALUES ('948', '3', '2016-06-18 08:31:12', '1', null, '10057', '0');
INSERT INTO yh_vote VALUES ('949', '3', '2016-06-18 08:31:16', '1', null, '10058', '0');
INSERT INTO yh_vote VALUES ('950', '2', '2016-06-18 08:31:18', '1', null, '10059', '0');
INSERT INTO yh_vote VALUES ('951', '2', '2016-06-18 08:31:19', '1', null, '10060', '0');
INSERT INTO yh_vote VALUES ('952', '3', '2016-06-18 08:31:20', '1', null, '10061', '0');
INSERT INTO yh_vote VALUES ('953', '3', '2016-06-18 08:31:20', '1', null, '10062', '0');
INSERT INTO yh_vote VALUES ('954', '3', '2016-06-18 08:31:26', '1', null, '10063', '0');
INSERT INTO yh_vote VALUES ('955', '2', '2016-06-18 08:31:29', '1', null, '10064', '0');
INSERT INTO yh_vote VALUES ('956', '2', '2016-06-18 08:31:30', '1', null, '10065', '0');
INSERT INTO yh_vote VALUES ('957', '1', '2016-06-18 08:36:15', '2', null, '10009', '0');
INSERT INTO yh_vote VALUES ('958', '2', '2016-06-18 08:36:17', '2', null, '10013', '0');
INSERT INTO yh_vote VALUES ('959', '1', '2016-06-18 08:36:19', '2', null, '10059', '0');
INSERT INTO yh_vote VALUES ('960', '1', '2016-06-18 08:36:21', '2', null, '10031', '0');
INSERT INTO yh_vote VALUES ('961', '2', '2016-06-18 08:36:21', '2', null, '10018', '0');
INSERT INTO yh_vote VALUES ('962', '2', '2016-06-18 08:36:21', '2', null, '10055', '0');
INSERT INTO yh_vote VALUES ('963', '2', '2016-06-18 08:36:21', '2', null, '10023', '0');
INSERT INTO yh_vote VALUES ('964', '2', '2016-06-18 08:36:22', '2', null, '10046', '0');
INSERT INTO yh_vote VALUES ('965', '2', '2016-06-18 08:36:23', '2', null, '10028', '0');
INSERT INTO yh_vote VALUES ('966', '2', '2016-06-18 08:36:23', '2', null, '10026', '0');
INSERT INTO yh_vote VALUES ('967', '2', '2016-06-18 08:36:23', '2', null, '10020', '0');
INSERT INTO yh_vote VALUES ('968', '1', '2016-06-18 08:36:25', '2', null, '10035', '0');
INSERT INTO yh_vote VALUES ('969', '2', '2016-06-18 08:36:26', '2', null, '10029', '0');
INSERT INTO yh_vote VALUES ('970', '2', '2016-06-18 08:36:28', '2', null, '10014', '0');
INSERT INTO yh_vote VALUES ('971', '1', '2016-06-18 08:36:30', '2', null, '10071', '0');
INSERT INTO yh_vote VALUES ('972', '1', '2016-06-18 08:36:30', '2', null, '10049', '0');
INSERT INTO yh_vote VALUES ('973', '5', '2016-06-18 08:36:30', '2', null, '10008', '0');
INSERT INTO yh_vote VALUES ('974', '2', '2016-06-18 08:36:30', '2', null, '10043', '0');
INSERT INTO yh_vote VALUES ('975', '2', '2016-06-18 08:36:31', '2', null, '10039', '0');
INSERT INTO yh_vote VALUES ('976', '1', '2016-06-18 08:36:31', '2', null, '10019', '0');
INSERT INTO yh_vote VALUES ('977', '1', '2016-06-18 08:36:31', '2', null, '10033', '0');
INSERT INTO yh_vote VALUES ('978', '2', '2016-06-18 08:36:31', '2', null, '10005', '0');
INSERT INTO yh_vote VALUES ('979', '2', '2016-06-18 08:36:32', '2', null, '10036', '0');
INSERT INTO yh_vote VALUES ('980', '1', '2016-06-18 08:36:32', '2', null, '10015', '0');
INSERT INTO yh_vote VALUES ('981', '2', '2016-06-18 08:36:32', '2', null, '10012', '0');
INSERT INTO yh_vote VALUES ('982', '1', '2016-06-18 08:36:33', '2', null, '10025', '0');
INSERT INTO yh_vote VALUES ('983', '1', '2016-06-18 08:36:33', '2', null, '10032', '0');
INSERT INTO yh_vote VALUES ('984', '2', '2016-06-18 08:36:33', '2', null, '10044', '0');
INSERT INTO yh_vote VALUES ('985', '2', '2016-06-18 08:36:34', '2', null, '10050', '0');
INSERT INTO yh_vote VALUES ('986', '2', '2016-06-18 08:36:34', '2', null, '10058', '0');
INSERT INTO yh_vote VALUES ('987', '2', '2016-06-18 08:36:35', '2', null, '10048', '0');
INSERT INTO yh_vote VALUES ('988', '2', '2016-06-18 08:36:35', '2', null, '10017', '0');
INSERT INTO yh_vote VALUES ('989', '2', '2016-06-18 08:36:37', '2', null, '10056', '0');
INSERT INTO yh_vote VALUES ('990', '2', '2016-06-18 08:36:38', '2', null, '10040', '0');
INSERT INTO yh_vote VALUES ('991', '1', '2016-06-18 08:36:39', '2', null, '10064', '0');
INSERT INTO yh_vote VALUES ('992', '2', '2016-06-18 08:36:40', '2', null, '10061', '0');
INSERT INTO yh_vote VALUES ('993', '2', '2016-06-18 08:36:43', '2', null, '10052', '0');
INSERT INTO yh_vote VALUES ('994', '2', '2016-06-18 08:36:46', '2', null, '10065', '0');
INSERT INTO yh_vote VALUES ('995', '2', '2016-06-18 08:36:50', '2', null, '10010', '0');
INSERT INTO yh_vote VALUES ('996', '1', '2016-06-18 08:36:51', '2', null, '10054', '0');
INSERT INTO yh_vote VALUES ('997', '2', '2016-06-18 08:36:52', '2', null, '10072', '0');
INSERT INTO yh_vote VALUES ('998', '2', '2016-06-18 08:36:52', '2', null, '10073', '0');
INSERT INTO yh_vote VALUES ('999', '2', '2016-06-18 08:36:52', '2', null, '10047', '0');
INSERT INTO yh_vote VALUES ('1000', '1', '2016-06-18 08:36:58', '2', null, '10074', '0');
INSERT INTO yh_vote VALUES ('1001', '2', '2016-06-18 08:37:03', '2', null, '10016', '0');
INSERT INTO yh_vote VALUES ('1002', '1', '2016-06-18 08:37:04', '2', null, '10022', '0');
INSERT INTO yh_vote VALUES ('1003', '1', '2016-06-18 08:37:06', '2', null, '10075', '0');
INSERT INTO yh_vote VALUES ('1004', '2', '2016-06-18 08:37:06', '2', null, '10076', '0');
INSERT INTO yh_vote VALUES ('1005', '2', '2016-06-18 08:37:06', '2', null, '10060', '0');
INSERT INTO yh_vote VALUES ('1006', '2', '2016-06-18 08:37:12', '2', null, '10063', '0');
INSERT INTO yh_vote VALUES ('1007', '2', '2016-06-18 08:37:15', '2', null, '10021', '0');
INSERT INTO yh_vote VALUES ('1008', '1', '2016-06-18 08:42:46', '3', null, '10055', '0');
INSERT INTO yh_vote VALUES ('1009', '1', '2016-06-18 08:42:48', '3', null, '10024', '0');
INSERT INTO yh_vote VALUES ('1010', '1', '2016-06-18 08:42:50', '3', null, '10075', '0');
INSERT INTO yh_vote VALUES ('1011', '1', '2016-06-18 08:42:50', '3', null, '10016', '0');
INSERT INTO yh_vote VALUES ('1012', '1', '2016-06-18 08:42:51', '3', null, '10033', '0');
INSERT INTO yh_vote VALUES ('1013', '1', '2016-06-18 08:42:53', '3', null, '10020', '0');
INSERT INTO yh_vote VALUES ('1014', '1', '2016-06-18 08:42:53', '3', null, '10029', '0');
INSERT INTO yh_vote VALUES ('1015', '1', '2016-06-18 08:42:54', '3', null, '10005', '0');
INSERT INTO yh_vote VALUES ('1016', '1', '2016-06-18 08:42:55', '3', null, '10019', '0');
INSERT INTO yh_vote VALUES ('1017', '1', '2016-06-18 08:42:55', '3', null, '10057', '0');
INSERT INTO yh_vote VALUES ('1018', '1', '2016-06-18 08:42:55', '3', null, '10047', '0');
INSERT INTO yh_vote VALUES ('1019', '1', '2016-06-18 08:42:55', '3', null, '10018', '0');
INSERT INTO yh_vote VALUES ('1020', '5', '2016-06-18 08:42:55', '3', null, '10035', '0');
INSERT INTO yh_vote VALUES ('1021', '1', '2016-06-18 08:42:56', '3', null, '10025', '0');
INSERT INTO yh_vote VALUES ('1022', '1', '2016-06-18 08:42:56', '3', null, '10058', '0');
INSERT INTO yh_vote VALUES ('1023', '1', '2016-06-18 08:42:56', '3', null, '10059', '0');
INSERT INTO yh_vote VALUES ('1024', '1', '2016-06-18 08:42:57', '3', null, '10011', '0');
INSERT INTO yh_vote VALUES ('1025', '1', '2016-06-18 08:42:57', '3', null, '10050', '0');
INSERT INTO yh_vote VALUES ('1026', '1', '2016-06-18 08:42:58', '3', null, '10060', '0');
INSERT INTO yh_vote VALUES ('1027', '3', '2016-06-18 08:42:58', '3', null, '10013', '0');
INSERT INTO yh_vote VALUES ('1028', '1', '2016-06-18 08:42:59', '3', null, '10077', '0');
INSERT INTO yh_vote VALUES ('1029', '1', '2016-06-18 08:42:59', '3', null, '10043', '0');
INSERT INTO yh_vote VALUES ('1030', '1', '2016-06-18 08:42:59', '3', null, '10034', '0');
INSERT INTO yh_vote VALUES ('1031', '1', '2016-06-18 08:43:00', '3', null, '10054', '0');
INSERT INTO yh_vote VALUES ('1032', '1', '2016-06-18 08:43:01', '3', null, '10032', '0');
INSERT INTO yh_vote VALUES ('1033', '1', '2016-06-18 08:43:02', '3', null, '10028', '0');
INSERT INTO yh_vote VALUES ('1034', '1', '2016-06-18 08:43:02', '3', null, '10039', '0');
INSERT INTO yh_vote VALUES ('1035', '1', '2016-06-18 08:43:04', '3', null, '10056', '0');
INSERT INTO yh_vote VALUES ('1036', '1', '2016-06-18 08:43:04', '3', null, '10022', '0');
INSERT INTO yh_vote VALUES ('1037', '1', '2016-06-18 08:43:05', '3', null, '10048', '0');
INSERT INTO yh_vote VALUES ('1038', '1', '2016-06-18 08:43:05', '3', null, '10023', '0');
INSERT INTO yh_vote VALUES ('1039', '1', '2016-06-18 08:43:05', '3', null, '10009', '0');
INSERT INTO yh_vote VALUES ('1040', '1', '2016-06-18 08:43:05', '3', null, '10064', '0');
INSERT INTO yh_vote VALUES ('1041', '1', '2016-06-18 08:43:06', '3', null, '10017', '0');
INSERT INTO yh_vote VALUES ('1042', '1', '2016-06-18 08:43:06', '3', null, '10026', '0');
INSERT INTO yh_vote VALUES ('1043', '1', '2016-06-18 08:43:06', '3', null, '10049', '0');
INSERT INTO yh_vote VALUES ('1044', '1', '2016-06-18 08:43:06', '3', null, '10044', '0');
INSERT INTO yh_vote VALUES ('1045', '3', '2016-06-18 08:43:07', '3', null, '10014', '0');
INSERT INTO yh_vote VALUES ('1046', '1', '2016-06-18 08:43:09', '3', null, '10015', '0');
INSERT INTO yh_vote VALUES ('1047', '1', '2016-06-18 08:43:10', '3', null, '10012', '0');
INSERT INTO yh_vote VALUES ('1048', '5', '2016-06-18 08:43:16', '3', null, '10074', '0');
INSERT INTO yh_vote VALUES ('1049', '1', '2016-06-18 08:43:16', '3', null, '10063', '0');
INSERT INTO yh_vote VALUES ('1050', '1', '2016-06-18 08:43:20', '3', null, '10073', '0');
INSERT INTO yh_vote VALUES ('1051', '1', '2016-06-18 08:43:21', '3', null, '10036', '0');
INSERT INTO yh_vote VALUES ('1052', '3', '2016-06-18 08:43:22', '3', null, '10010', '0');
INSERT INTO yh_vote VALUES ('1053', '1', '2016-06-18 08:43:25', '3', null, '10031', '0');
INSERT INTO yh_vote VALUES ('1054', '1', '2016-06-18 08:43:26', '3', null, '10065', '0');
INSERT INTO yh_vote VALUES ('1055', '4', '2016-06-18 08:43:34', '3', null, '10046', '0');
INSERT INTO yh_vote VALUES ('1056', '5', '2016-06-18 08:43:36', '3', null, '10021', '0');
INSERT INTO yh_vote VALUES ('1057', '1', '2016-06-18 09:37:40', '4', null, '10033', '0');
INSERT INTO yh_vote VALUES ('1058', '1', '2016-06-18 09:37:40', '4', null, '10016', '0');
INSERT INTO yh_vote VALUES ('1059', '4', '2016-06-18 09:37:42', '4', null, '10003', '0');
INSERT INTO yh_vote VALUES ('1060', '1', '2016-06-18 09:37:44', '4', null, '10020', '0');
INSERT INTO yh_vote VALUES ('1061', '1', '2016-06-18 09:37:45', '4', null, '10055', '0');
INSERT INTO yh_vote VALUES ('1062', '1', '2016-06-18 09:37:45', '4', null, '10017', '0');
INSERT INTO yh_vote VALUES ('1063', '1', '2016-06-18 09:37:45', '4', null, '10019', '0');
INSERT INTO yh_vote VALUES ('1064', '1', '2016-06-18 09:37:46', '4', null, '10023', '0');
INSERT INTO yh_vote VALUES ('1065', '1', '2016-06-18 09:37:47', '4', null, '10028', '0');
INSERT INTO yh_vote VALUES ('1066', '1', '2016-06-18 09:37:47', '4', null, '10058', '0');
INSERT INTO yh_vote VALUES ('1067', '4', '2016-06-18 09:37:49', '4', null, '10059', '0');
INSERT INTO yh_vote VALUES ('1068', '1', '2016-06-18 09:37:49', '4', null, '10013', '0');
INSERT INTO yh_vote VALUES ('1069', '1', '2016-06-18 09:37:54', '4', null, '10031', '0');
INSERT INTO yh_vote VALUES ('1070', '2', '2016-06-18 09:37:54', '4', null, '10085', '0');
INSERT INTO yh_vote VALUES ('1071', '1', '2016-06-18 09:37:54', '4', null, '10029', '0');
INSERT INTO yh_vote VALUES ('1072', '1', '2016-06-18 09:37:55', '4', null, '10025', '0');
INSERT INTO yh_vote VALUES ('1073', '1', '2016-06-18 09:37:55', '4', null, '10047', '0');
INSERT INTO yh_vote VALUES ('1074', '2', '2016-06-18 09:37:56', '4', null, '10015', '0');
INSERT INTO yh_vote VALUES ('1075', '2', '2016-06-18 09:37:56', '4', null, '10005', '0');
INSERT INTO yh_vote VALUES ('1076', '1', '2016-06-18 09:37:56', '4', null, '10065', '0');
INSERT INTO yh_vote VALUES ('1077', '1', '2016-06-18 09:37:59', '4', null, '10043', '0');
INSERT INTO yh_vote VALUES ('1078', '1', '2016-06-18 09:37:59', '4', null, '10036', '0');
INSERT INTO yh_vote VALUES ('1079', '2', '2016-06-18 09:38:00', '4', null, '10054', '0');
INSERT INTO yh_vote VALUES ('1080', '2', '2016-06-18 09:38:01', '4', null, '10075', '0');
INSERT INTO yh_vote VALUES ('1081', '1', '2016-06-18 09:38:01', '4', null, '10018', '0');
INSERT INTO yh_vote VALUES ('1082', '4', '2016-06-18 09:38:02', '4', null, '10074', '0');
INSERT INTO yh_vote VALUES ('1083', '2', '2016-06-18 09:38:04', '4', null, '10024', '0');
INSERT INTO yh_vote VALUES ('1084', '1', '2016-06-18 09:38:05', '4', null, '10063', '0');
INSERT INTO yh_vote VALUES ('1085', '4', '2016-06-18 09:38:05', '4', null, '10062', '0');
INSERT INTO yh_vote VALUES ('1086', '1', '2016-06-18 09:38:06', '4', null, '10086', '0');
INSERT INTO yh_vote VALUES ('1087', '1', '2016-06-18 09:38:08', '4', null, '10087', '0');
INSERT INTO yh_vote VALUES ('1088', '1', '2016-06-18 09:38:08', '4', null, '10026', '0');
INSERT INTO yh_vote VALUES ('1089', '1', '2016-06-18 09:38:10', '4', null, '10044', '0');
INSERT INTO yh_vote VALUES ('1090', '1', '2016-06-18 09:38:13', '4', null, '10061', '0');
INSERT INTO yh_vote VALUES ('1091', '1', '2016-06-18 09:38:16', '4', null, '10072', '0');
INSERT INTO yh_vote VALUES ('1092', '1', '2016-06-18 09:38:19', '4', null, '10046', '0');
INSERT INTO yh_vote VALUES ('1093', '1', '2016-06-18 09:38:20', '4', null, '10088', '0');
INSERT INTO yh_vote VALUES ('1094', '1', '2016-06-18 09:38:23', '4', null, '10073', '0');
INSERT INTO yh_vote VALUES ('1095', '3', '2016-06-18 09:38:24', '4', null, '10052', '0');
INSERT INTO yh_vote VALUES ('1096', '1', '2016-06-18 09:38:30', '4', null, '10089', '0');
INSERT INTO yh_vote VALUES ('1097', '2', '2016-06-18 09:38:38', '4', null, '10090', '0');
INSERT INTO yh_vote VALUES ('1098', '3', '2016-06-18 09:41:09', '5', null, '10003', '0');
INSERT INTO yh_vote VALUES ('1099', '3', '2016-06-18 09:41:10', '5', null, '10054', '0');
INSERT INTO yh_vote VALUES ('1100', '3', '2016-06-18 09:41:10', '5', null, '10090', '0');
INSERT INTO yh_vote VALUES ('1101', '1', '2016-06-18 09:41:10', '5', null, '10055', '0');
INSERT INTO yh_vote VALUES ('1102', '1', '2016-06-18 09:41:12', '5', null, '10005', '0');
INSERT INTO yh_vote VALUES ('1103', '1', '2016-06-18 09:41:13', '5', null, '10043', '0');
INSERT INTO yh_vote VALUES ('1104', '1', '2016-06-18 09:41:13', '5', null, '10033', '0');
INSERT INTO yh_vote VALUES ('1105', '2', '2016-06-18 09:41:13', '5', null, '10091', '0');
INSERT INTO yh_vote VALUES ('1106', '1', '2016-06-18 09:41:13', '5', null, '10015', '0');
INSERT INTO yh_vote VALUES ('1107', '3', '2016-06-18 09:41:15', '5', null, '10024', '0');
INSERT INTO yh_vote VALUES ('1108', '1', '2016-06-18 09:41:16', '5', null, '10019', '0');
INSERT INTO yh_vote VALUES ('1109', '3', '2016-06-18 09:41:17', '5', null, '10020', '0');
INSERT INTO yh_vote VALUES ('1110', '1', '2016-06-18 09:41:17', '5', null, '10028', '0');
INSERT INTO yh_vote VALUES ('1111', '3', '2016-06-18 09:41:18', '5', null, '10062', '0');
INSERT INTO yh_vote VALUES ('1112', '1', '2016-06-18 09:41:19', '5', null, '10085', '0');
INSERT INTO yh_vote VALUES ('1113', '1', '2016-06-18 09:41:20', '5', null, '10026', '0');
INSERT INTO yh_vote VALUES ('1114', '3', '2016-06-18 09:41:20', '5', null, '10052', '0');
INSERT INTO yh_vote VALUES ('1115', '3', '2016-06-18 09:41:21', '5', null, '10059', '0');
INSERT INTO yh_vote VALUES ('1116', '1', '2016-06-18 09:41:21', '5', null, '10075', '0');
INSERT INTO yh_vote VALUES ('1117', '1', '2016-06-18 09:41:21', '5', null, '10046', '0');
INSERT INTO yh_vote VALUES ('1118', '1', '2016-06-18 09:41:23', '5', null, '10092', '0');
INSERT INTO yh_vote VALUES ('1119', '3', '2016-06-18 09:41:23', '5', null, '10031', '0');
INSERT INTO yh_vote VALUES ('1120', '1', '2016-06-18 09:41:23', '5', null, '10093', '0');
INSERT INTO yh_vote VALUES ('1121', '1', '2016-06-18 09:41:23', '5', null, '10058', '0');
INSERT INTO yh_vote VALUES ('1122', '1', '2016-06-18 09:41:23', '5', null, '10073', '0');
INSERT INTO yh_vote VALUES ('1123', '1', '2016-06-18 09:41:23', '5', null, '10074', '0');
INSERT INTO yh_vote VALUES ('1124', '3', '2016-06-18 09:41:25', '5', null, '10025', '0');
INSERT INTO yh_vote VALUES ('1125', '1', '2016-06-18 09:41:25', '5', null, '10013', '0');
INSERT INTO yh_vote VALUES ('1126', '3', '2016-06-18 09:41:26', '5', null, '10016', '0');
INSERT INTO yh_vote VALUES ('1127', '2', '2016-06-18 09:41:29', '5', null, '10094', '0');
INSERT INTO yh_vote VALUES ('1128', '3', '2016-06-18 09:41:30', '5', null, '10018', '0');
INSERT INTO yh_vote VALUES ('1129', '2', '2016-06-18 09:41:32', '5', null, '10095', '0');
INSERT INTO yh_vote VALUES ('1130', '3', '2016-06-18 09:41:33', '5', null, '10023', '0');
INSERT INTO yh_vote VALUES ('1131', '3', '2016-06-18 09:41:35', '5', null, '10087', '0');
INSERT INTO yh_vote VALUES ('1132', '2', '2016-06-18 09:41:35', '5', null, '10096', '0');
INSERT INTO yh_vote VALUES ('1133', '1', '2016-06-18 09:41:38', '5', null, '10047', '0');
INSERT INTO yh_vote VALUES ('1134', '3', '2016-06-18 09:41:53', '5', null, '10014', '0');
INSERT INTO yh_vote VALUES ('1135', '1', '2016-06-18 09:41:53', '5', null, '10048', '0');
INSERT INTO yh_vote VALUES ('1136', '1', '2016-06-18 09:41:55', '5', null, '10097', '0');
INSERT INTO yh_vote VALUES ('1137', '1', '2016-06-18 09:41:56', '5', null, '10061', '0');
INSERT INTO yh_vote VALUES ('1138', '1', '2016-06-18 09:42:08', '5', null, '10017', '0');
INSERT INTO yh_vote VALUES ('1139', '1', '2016-06-18 09:45:43', '6', null, '10055', '0');
INSERT INTO yh_vote VALUES ('1140', '2', '2016-06-18 09:45:47', '6', null, '10005', '0');
INSERT INTO yh_vote VALUES ('1141', '1', '2016-06-18 09:45:48', '6', null, '10016', '0');
INSERT INTO yh_vote VALUES ('1142', '3', '2016-06-18 09:45:51', '6', null, '10054', '0');
INSERT INTO yh_vote VALUES ('1143', '2', '2016-06-18 09:45:51', '6', null, '10015', '0');
INSERT INTO yh_vote VALUES ('1144', '1', '2016-06-18 09:45:51', '6', null, '10023', '0');
INSERT INTO yh_vote VALUES ('1145', '2', '2016-06-18 09:45:51', '6', null, '10020', '0');
INSERT INTO yh_vote VALUES ('1146', '1', '2016-06-18 09:45:52', '6', null, '10075', '0');
INSERT INTO yh_vote VALUES ('1147', '2', '2016-06-18 09:45:53', '6', null, '10043', '0');
INSERT INTO yh_vote VALUES ('1148', '2', '2016-06-18 09:45:53', '6', null, '10097', '0');
INSERT INTO yh_vote VALUES ('1149', '1', '2016-06-18 09:45:53', '6', null, '10062', '0');
INSERT INTO yh_vote VALUES ('1150', '2', '2016-06-18 09:45:55', '6', null, '10017', '0');
INSERT INTO yh_vote VALUES ('1151', '2', '2016-06-18 09:45:55', '6', null, '10059', '0');
INSERT INTO yh_vote VALUES ('1152', '2', '2016-06-18 09:45:56', '6', null, '10047', '0');
INSERT INTO yh_vote VALUES ('1153', '2', '2016-06-18 09:45:56', '6', null, '10013', '0');
INSERT INTO yh_vote VALUES ('1154', '2', '2016-06-18 09:46:00', '6', null, '10028', '0');
INSERT INTO yh_vote VALUES ('1155', '1', '2016-06-18 09:46:01', '6', null, '10085', '0');
INSERT INTO yh_vote VALUES ('1156', '2', '2016-06-18 09:46:02', '6', null, '10024', '0');
INSERT INTO yh_vote VALUES ('1157', '1', '2016-06-18 09:46:02', '6', null, '10058', '0');
INSERT INTO yh_vote VALUES ('1158', '2', '2016-06-18 09:46:02', '6', null, '10033', '0');
INSERT INTO yh_vote VALUES ('1159', '2', '2016-06-18 09:46:03', '6', null, '10014', '0');
INSERT INTO yh_vote VALUES ('1160', '2', '2016-06-18 09:46:03', '6', null, '10093', '0');
INSERT INTO yh_vote VALUES ('1161', '2', '2016-06-18 09:46:04', '6', null, '10025', '0');
INSERT INTO yh_vote VALUES ('1162', '1', '2016-06-18 09:46:04', '6', null, '10019', '0');
INSERT INTO yh_vote VALUES ('1163', '2', '2016-06-18 09:46:05', '6', null, '10073', '0');
INSERT INTO yh_vote VALUES ('1164', '4', '2016-06-18 09:46:06', '6', null, '10090', '0');
INSERT INTO yh_vote VALUES ('1165', '2', '2016-06-18 09:46:06', '6', null, '10048', '0');
INSERT INTO yh_vote VALUES ('1166', '2', '2016-06-18 09:46:08', '6', null, '10003', '0');
INSERT INTO yh_vote VALUES ('1167', '2', '2016-06-18 09:46:08', '6', null, '10036', '0');
INSERT INTO yh_vote VALUES ('1168', '1', '2016-06-18 09:46:12', '6', null, '10052', '0');
INSERT INTO yh_vote VALUES ('1169', '2', '2016-06-18 09:46:12', '6', null, '10018', '0');
INSERT INTO yh_vote VALUES ('1170', '1', '2016-06-18 09:46:17', '6', null, '10074', '0');
INSERT INTO yh_vote VALUES ('1171', '1', '2016-06-18 09:46:20', '6', null, '10031', '0');
INSERT INTO yh_vote VALUES ('1172', '1', '2016-06-18 09:46:22', '6', null, '10008', '0');
INSERT INTO yh_vote VALUES ('1173', '2', '2016-06-18 09:46:26', '6', null, '10065', '0');
INSERT INTO yh_vote VALUES ('1174', '3', '2016-06-18 09:46:32', '6', null, '10087', '0');
INSERT INTO yh_vote VALUES ('1175', '1', '2016-06-18 09:46:35', '6', null, '10046', '0');
INSERT INTO yh_vote VALUES ('1176', '2', '2016-06-18 10:48:22', '7', null, '10014', '0');
INSERT INTO yh_vote VALUES ('1177', '2', '2016-06-18 10:48:35', '7', null, '10047', '0');
INSERT INTO yh_vote VALUES ('1178', '2', '2016-06-18 10:48:37', '7', null, '10017', '0');
INSERT INTO yh_vote VALUES ('1179', '2', '2016-06-18 10:48:37', '7', null, '10019', '0');
INSERT INTO yh_vote VALUES ('1180', '2', '2016-06-18 10:48:37', '7', null, '10020', '0');
INSERT INTO yh_vote VALUES ('1181', '2', '2016-06-18 10:48:38', '7', null, '10097', '0');
INSERT INTO yh_vote VALUES ('1182', '2', '2016-06-18 10:48:39', '7', null, '10033', '0');
INSERT INTO yh_vote VALUES ('1183', '2', '2016-06-18 10:48:41', '7', null, '10073', '0');
INSERT INTO yh_vote VALUES ('1184', '2', '2016-06-18 10:48:43', '7', null, '10039', '0');
INSERT INTO yh_vote VALUES ('1185', '3', '2016-06-18 10:48:44', '7', null, '10089', '0');
INSERT INTO yh_vote VALUES ('1186', '2', '2016-06-18 10:48:49', '7', null, '10055', '0');
INSERT INTO yh_vote VALUES ('1187', '2', '2016-06-18 10:48:50', '7', null, '10029', '0');
INSERT INTO yh_vote VALUES ('1188', '1', '2016-06-18 10:48:51', '7', null, '10010', '0');
INSERT INTO yh_vote VALUES ('1189', '1', '2016-06-18 10:48:51', '7', null, '10074', '0');
INSERT INTO yh_vote VALUES ('1190', '2', '2016-06-18 10:48:54', '7', null, '10058', '0');
INSERT INTO yh_vote VALUES ('1191', '2', '2016-06-18 10:48:55', '7', null, '10076', '0');
INSERT INTO yh_vote VALUES ('1192', '2', '2016-06-18 10:48:56', '7', null, '10008', '0');
INSERT INTO yh_vote VALUES ('1193', '2', '2016-06-18 10:48:58', '7', null, '10003', '0');
INSERT INTO yh_vote VALUES ('1194', '2', '2016-06-18 10:49:00', '7', null, '10099', '0');
INSERT INTO yh_vote VALUES ('1195', '1', '2016-06-18 10:49:01', '7', null, '10057', '0');
INSERT INTO yh_vote VALUES ('1196', '1', '2016-06-18 10:49:03', '7', null, '10088', '0');
INSERT INTO yh_vote VALUES ('1197', '2', '2016-06-18 10:49:06', '7', null, '10016', '0');
INSERT INTO yh_vote VALUES ('1198', '2', '2016-06-18 10:49:09', '7', null, '10100', '0');
INSERT INTO yh_vote VALUES ('1199', '2', '2016-06-18 10:49:09', '7', null, '10077', '0');
INSERT INTO yh_vote VALUES ('1200', '2', '2016-06-18 10:49:09', '7', null, '10093', '0');
INSERT INTO yh_vote VALUES ('1201', '2', '2016-06-18 10:49:18', '7', null, '10062', '0');
INSERT INTO yh_vote VALUES ('1202', '2', '2016-06-18 10:49:19', '7', null, '10028', '0');
INSERT INTO yh_vote VALUES ('1203', '2', '2016-06-18 10:49:30', '7', null, '10071', '0');
INSERT INTO yh_vote VALUES ('1204', '1', '2016-06-18 10:55:26', '8', null, '10024', '0');
INSERT INTO yh_vote VALUES ('1205', '2', '2016-06-18 10:55:31', '8', null, '10014', '0');
INSERT INTO yh_vote VALUES ('1206', '2', '2016-06-18 10:55:32', '8', null, '10019', '0');
INSERT INTO yh_vote VALUES ('1207', '2', '2016-06-18 10:55:34', '8', null, '10039', '0');
INSERT INTO yh_vote VALUES ('1208', '1', '2016-06-18 10:55:36', '8', null, '10055', '0');
INSERT INTO yh_vote VALUES ('1209', '2', '2016-06-18 10:55:40', '8', null, '10033', '0');
INSERT INTO yh_vote VALUES ('1210', '4', '2016-06-18 10:55:41', '8', null, '10074', '0');
INSERT INTO yh_vote VALUES ('1211', '4', '2016-06-18 10:55:41', '8', null, '10008', '0');
INSERT INTO yh_vote VALUES ('1212', '2', '2016-06-18 10:55:42', '8', null, '10097', '0');
INSERT INTO yh_vote VALUES ('1213', '2', '2016-06-18 10:55:43', '8', null, '10088', '0');
INSERT INTO yh_vote VALUES ('1214', '1', '2016-06-18 10:55:45', '8', null, '10100', '0');
INSERT INTO yh_vote VALUES ('1215', '4', '2016-06-18 10:55:48', '8', null, '10018', '0');
INSERT INTO yh_vote VALUES ('1216', '1', '2016-06-18 10:55:48', '8', null, '10101', '0');
INSERT INTO yh_vote VALUES ('1217', '2', '2016-06-18 10:55:49', '8', null, '10057', '0');
INSERT INTO yh_vote VALUES ('1218', '2', '2016-06-18 10:55:51', '8', null, '10058', '0');
INSERT INTO yh_vote VALUES ('1219', '1', '2016-06-18 10:55:51', '8', null, '10093', '0');
INSERT INTO yh_vote VALUES ('1220', '2', '2016-06-18 10:55:52', '8', null, '10087', '0');
INSERT INTO yh_vote VALUES ('1221', '1', '2016-06-18 10:55:54', '8', null, '10062', '0');
INSERT INTO yh_vote VALUES ('1222', '2', '2016-06-18 10:55:56', '8', null, '10010', '0');
INSERT INTO yh_vote VALUES ('1223', '2', '2016-06-18 10:55:56', '8', null, '10017', '0');
INSERT INTO yh_vote VALUES ('1224', '2', '2016-06-18 10:55:57', '8', null, '10026', '0');
INSERT INTO yh_vote VALUES ('1225', '1', '2016-06-18 10:55:58', '8', null, '10028', '0');
INSERT INTO yh_vote VALUES ('1226', '2', '2016-06-18 10:56:03', '8', null, '10025', '0');
INSERT INTO yh_vote VALUES ('1227', '1', '2016-06-18 10:56:03', '8', null, '10012', '0');
INSERT INTO yh_vote VALUES ('1228', '2', '2016-06-18 10:56:04', '8', null, '10047', '0');
INSERT INTO yh_vote VALUES ('1229', '3', '2016-06-18 10:56:09', '8', null, '10099', '0');
INSERT INTO yh_vote VALUES ('1230', '3', '2016-06-18 10:56:14', '8', null, '10059', '0');
INSERT INTO yh_vote VALUES ('1231', '2', '2016-06-18 10:56:22', '8', null, '10054', '0');
INSERT INTO yh_vote VALUES ('1232', '1', '2016-06-18 10:59:39', '9', null, '10055', '0');
INSERT INTO yh_vote VALUES ('1233', '1', '2016-06-18 10:59:40', '9', null, '10088', '0');
INSERT INTO yh_vote VALUES ('1234', '1', '2016-06-18 10:59:41', '9', null, '10014', '0');
INSERT INTO yh_vote VALUES ('1235', '3', '2016-06-18 10:59:42', '9', null, '10100', '0');
INSERT INTO yh_vote VALUES ('1236', '1', '2016-06-18 10:59:44', '9', null, '10020', '0');
INSERT INTO yh_vote VALUES ('1237', '3', '2016-06-18 10:59:45', '9', null, '10033', '0');
INSERT INTO yh_vote VALUES ('1238', '1', '2016-06-18 10:59:46', '9', null, '10025', '0');
INSERT INTO yh_vote VALUES ('1239', '3', '2016-06-18 10:59:50', '9', null, '10047', '0');
INSERT INTO yh_vote VALUES ('1240', '1', '2016-06-18 10:59:52', '9', null, '10026', '0');
INSERT INTO yh_vote VALUES ('1241', '1', '2016-06-18 10:59:52', '9', null, '10097', '0');
INSERT INTO yh_vote VALUES ('1242', '1', '2016-06-18 10:59:55', '9', null, '10010', '0');
INSERT INTO yh_vote VALUES ('1243', '1', '2016-06-18 10:59:58', '9', null, '10062', '0');
INSERT INTO yh_vote VALUES ('1244', '3', '2016-06-18 11:00:02', '9', null, '10039', '0');
INSERT INTO yh_vote VALUES ('1245', '3', '2016-06-18 11:00:03', '9', null, '10058', '0');
INSERT INTO yh_vote VALUES ('1246', '3', '2016-06-18 11:00:04', '9', null, '10054', '0');
INSERT INTO yh_vote VALUES ('1247', '1', '2016-06-18 11:00:04', '9', null, '10057', '0');
INSERT INTO yh_vote VALUES ('1248', '3', '2016-06-18 11:00:05', '9', null, '10084', '0');
INSERT INTO yh_vote VALUES ('1249', '3', '2016-06-18 11:00:05', '9', null, '10093', '0');
INSERT INTO yh_vote VALUES ('1250', '1', '2016-06-18 11:00:06', '9', null, '10028', '0');
INSERT INTO yh_vote VALUES ('1251', '3', '2016-06-18 11:00:09', '9', null, '10101', '0');
INSERT INTO yh_vote VALUES ('1252', '3', '2016-06-18 11:00:09', '9', null, '10087', '0');
INSERT INTO yh_vote VALUES ('1253', '1', '2016-06-18 11:00:11', '9', null, '10017', '0');
INSERT INTO yh_vote VALUES ('1254', '1', '2016-06-18 11:00:11', '9', null, '10019', '0');
INSERT INTO yh_vote VALUES ('1255', '3', '2016-06-18 11:00:12', '9', null, '10024', '0');
INSERT INTO yh_vote VALUES ('1256', '3', '2016-06-18 11:00:13', '9', null, '10089', '0');
INSERT INTO yh_vote VALUES ('1257', '1', '2016-06-18 11:00:16', '9', null, '10099', '0');
INSERT INTO yh_vote VALUES ('1258', '1', '2016-06-18 11:00:27', '9', null, '10012', '0');
INSERT INTO yh_vote VALUES ('1259', '3', '2016-06-18 11:00:28', '9', null, '10059', '0');
